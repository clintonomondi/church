<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/getLineChartData', 'ApiController@getLineChartData')->name('getLineChartData');
//categories
Route::post('/category', 'CategoryController@postcategory')->name('postcategory');
Route::get('/categories', 'CategoryController@categories')->name('categories');
Route::post('/category/update/{id}', 'CategoryController@updatecat')->name('updatecat');
Route::get('/category/mebembers/{id}', 'CategoryController@getMembers')->name('getMembers');

Route::get('/getZones', 'ApiController@getZones')->name('getZones');

//members
Route::post('/members', 'MemberController@postmember')->name('postmember');
Route::get('/members', 'MemberController@members')->name('members');
Route::get('/members/search', 'MemberController@search')->name('search');
Route::post('/members/search', 'MemberController@postSearch')->name('postSearch');
Route::post('/viewmember/{id}/update', 'MemberController@updatemember')->name('updatemember');
Route::post('/members/comment/{id}', 'MemberController@postComment')->name('postComment');
Route::get('/comments', 'MemberController@comments')->name('comments');
Route::get('/member/{id}', 'MemberController@memberview')->name('memberview');
Route::get('/members/add', 'MemberController@addmember')->name('addmember');

//roles
Route::get('/users', 'RoleController@users')->name('users');
Route::post('/users', 'RoleController@postRole')->name('postRole');
Route::post('/users/update', 'RoleController@updateRole')->name('updateRole');
Route::get('/users/remove/{id}', 'RoleController@removeUser')->name('removeUser');
Route::post('/users/password', 'RoleController@postPassword')->name('postPassword');

//offeringd
Route::get('/offering', 'OfferingController@give')->name('give');
Route::post('/offering', 'OfferingController@postoffering')->name('postoffering');
Route::get('/offerings', 'OfferingController@offerings')->name('offerings');
Route::get('/offering/{id}', 'OfferingController@editoffer')->name('editoffer');
Route::post('/offering/{id}/update', 'OfferingController@updateoffer')->name('updateoffer');

//password
Route::get('/password', 'HomeController@password')->name('password');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/profile', 'HomeController@updateprofile')->name('updateprofile');
Route::post('/password', 'HomeController@postpassword')->name('postpassword');

//report
Route::get('/per_person', 'ReportController@per_person')->name('per_person');
Route::post('/per_person', 'ReportController@post_per_person')->name('post_per_person');

Route::get('/per_zone', 'ReportController@per_zone')->name('per_zone');
Route::post('/per_zone', 'ReportController@post_per_zone')->name('post_per_zone');
Route::post('/perzone/more/{id}', 'ReportController@perzone_more')->name('perzone_more');

Route::get('/per_service', 'ReportController@per_service')->name('per_service');
Route::post('/per_service', 'ReportController@post_per_service')->name('post_per_service');

Route::get('/getChartdata', 'ApiController@getChartdata')->name('getChartdata');
Route::get('/chartpage', 'ChartController@chartpage')->name('chartpage');
Route::get('/attendance', 'AttendanceController@attendance')->name('attendance');
Route::post('/attendance', 'AttendanceController@postattendance')->name('postattendance');
Route::get('/attendance/view/{id}', 'AttendanceController@view')->name('attendance.view');
Route::get('/attendance/record', 'AttendanceController@recordsummery')->name('recordsummery');
Route::get('/attendance/record/step2/{id}', 'AttendanceController@record2')->name('record2');
Route::post('/attendance/record/step2/{id}', 'AttendanceController@postsummery')->name('postsummery');
Route::post('/searchsummery', 'AttendanceController@searchsummery')->name('searchsummery');


Route::get('/apendusers', 'MemberController@apendusers')->name('apendusers');
Route::post('/searchofferings', 'OfferingController@searchofferings')->name('searchofferings');

Route::get('/doUpdate', 'MemberController@doUpdate')->name('doUpdate');


Route::get('/test', 'TestController@test')->name('test');
Route::post('/getTest', 'TestController@getCountry')->name('getCountry');

Route::post('/updateavarter', 'HomeController@updateavarter')->name('updateavarter');

Route::get('/pledges', 'PledgeController@pledges')->name('pledges');
Route::get('/pledges/add', 'PledgeController@addpledges')->name('addpledges');
Route::post('/pledges', 'PledgeController@postpledges')->name('postpledges');
Route::post('/pledges/pay/{id}', 'PledgeController@pay')->name('pay');
Route::get('/pledges/more/{id}', 'PledgeController@more')->name('more');




