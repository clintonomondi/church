<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fname');
            $table->string('lname');
            $table->string('regno')->nullable();
            $table->string('marital')->nullable();
            $table->string('dob')->nullable();
            $table->string('phone')->nullable();
            $table->string('gender')->nullable();
            $table->string('ministry')->nullable();
            $table->string('born')->nullable();
            $table->string('residence')->nullable();
            $table->string('church')->nullable();
            $table->string('email')->nullable();
            $table->string('parentName')->nullable();
            $table->string('parentPhone')->nullable();
            $table->string('ministerial')->nullable();
            $table->string('category_id');
            $table->string('user_id')->nullable();
            $table->string('spouse')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
