<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $social_users=\App\Offering::selectRaw('count(offeringtype) as count,id')->groupBy('id')->get();
        $user=[];

        foreach ($social_users as $result) {
            $user[$result->source]=(int)$result->count;
        }
        View::share('user', $user);
    }
}
