<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected  $fillable=['name','description','paster'];

    public  function member(){
        return $this->hasMany(Member::class);
    }

    public  function offering(){
        return $this->hasMany(Offering::class);
    }
}
