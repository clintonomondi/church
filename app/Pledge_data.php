<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pledge_data extends Model
{
    protected  $fillable=['pledge_id','amount','type','date'];

    public  function pledge(){
        return $this->belongsTo(Pledge::class);
    }
}
