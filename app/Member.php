<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected  $fillable=['fname','lname','regno','marital','dob','phone','gender','ministry','user_id','born','residence','church','email','parentName','parentPhone','ministerial','category_id','spouse'];

    public  function category(){
        return $this->belongsTo(Category::class);
    }
    public  function user(){
        return $this->belongsTo(User::class);
    }

    public  function  offering(){
        return $this->hasMany(Offering::class);
    }

    public  function  comment(){
        return $this->hasMany(Comment::class);
    }

    public  function pledge(){
        return $this->hasMany(Pledge::class);
    }
}
