<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Summery extends Model
{
    protected  $fillable=['giving','amount','attenance_id'];

    public  function attenance(){
        return $this->belongsTo(Attenance::class);
    }
}
