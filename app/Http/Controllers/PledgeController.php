<?php

namespace App\Http\Controllers;

use App\Pledge;
use App\Pledge_data;
use Illuminate\Http\Request;

class PledgeController extends Controller
{
public  function pledges(){
    $datas=Pledge::orderBy('id','desc')->get();
    return view('pledge.pledges',compact('datas'));
}

public  function addpledges(){
    return view('pledge.addpledge');
}

public  function postpledges(Request $request){

    $validatedData = $request->validate([
        'date' => 'required',
        'amount' => 'required',
        'member_id' => 'required',
        'type' => 'required',
    ]);
    $toDate = date("Y-m-d", strtotime($request->date));
    $request['date']=$toDate;
    $data=Pledge::create($request->all());
    return redirect()->back()->with('success','Pledge recorded successfully');
}

public  function pay(Request $request,$id){
   if($request->amount<=0){
       return redirect()->back()->with('error','Invalid amount');
   }
    $toDate = date("Y-m-d", strtotime($request->date));
    $request['date']=$toDate;
    $request['pledge_id']=$id;
    $request['type']='Payment';
    $data=Pledge_data::create($request->all());
    return redirect()->back()->with('success','Payment made successfully');

}

public  function more($id){
    $pledge=Pledge::find($id);
    $pledgedata=Pledge_data::where('pledge_id',$id)->get();

    return view('pledge.more',compact('pledge','pledgedata'));
}
}
