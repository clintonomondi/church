<?php

namespace App\Http\Controllers;

use App\Category;
use App\Member;
use App\Offering;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Array_;
use function MongoDB\BSON\toJSON;

class ReportController extends Controller
{


        public function per_person(){
            $datas=[];
            return view('reports.per_person',compact('datas'));

        }

        public  function post_per_person(Request $request){
            $from=$request->input('from');
            $to=$request->input('to');
            $toDate = date("d/m/Y", strtotime($to));
			 $fromDate = date("d/m/Y", strtotime($from));
          $datas=Offering::whereBetween('created_at', [$from, $to])->where('member_id',$request->member_id)->get();

            $member=Member::find($request->member_id);
            return view('reports.per_person',compact('datas','member','from','to'));

        }

        public  function per_service(){
            $datas=[];
            return view('reports.per_service',compact('datas'));
        }
        public  function  post_per_service(Request $request){
            $from=$request->input('from');
            $to=$request->input('to');
            $toDate = date("d/m/Y", strtotime($to));
            $service=$request->input('service');
            $datas=Offering::whereBetween('date', [$from, $to])->where('service',$service)->get();

            return view('reports.per_service',compact('datas','service','to','from'));

        }

    public  function per_zone(){
        $datas=Category::all();
        return view('reports.per_zone',compact('datas'));
    }

    public  function post_per_zone(Request $request){
        $from=$request->input('from');
        $to=$request->input('to');
        $toDate = date("d/m/Y", strtotime($to));
        $datas=Category::all();
        return view('reports.per_zone',compact('datas','to','from'));

    }

    public  function perzone_more(Request $request,$id){
$zone=Category::find($id);
$to=$request->to;
$from=$request->from;
$toDate = date("d/m/Y", strtotime($to));
        $datas = DB::select( DB::raw("SELECT DISTINCT offeringtype ,
(SELECT SUM(item) FROM offerings B WHERE B.offeringtype=A.offeringtype AND category_id='$id' AND date between '$from' AND '$toDate')sum
FROM `offerings` A WHERE category_id='$id' AND created_at between '$from' AND '$to'") );
        return view('reports.perzone_more',compact('datas','to','from','zone'));
    }


}
