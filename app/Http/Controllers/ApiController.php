<?php

namespace App\Http\Controllers;

use App\Attenance;
use App\Category;
use App\Member;
use App\Offering;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Array_;

class ApiController extends Controller
{
    public  function getLineChartData(){
        $year=date("Y");
        $query = DB::select( DB::raw("SELECT (SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 1 AND YEAR(created_at) = '$year')january,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 2  AND YEAR(created_at) ='$year')february,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 3  AND YEAR(created_at) ='$year' )march,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 4  AND YEAR(created_at) = '$year')april,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 5  AND YEAR(created_at) ='$year' )may,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 6  AND YEAR(created_at) ='$year' )june,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 7 AND YEAR(created_at) ='$year' )july,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 8 AND YEAR(created_at) ='$year' )augast,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 9 AND YEAR(created_at) ='$year')september,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 10 AND YEAR(created_at) ='$year' )october,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 11 AND YEAR(created_at) ='$year' )november,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE MONTH(created_at) = 12 AND YEAR(created_at) ='$year' )december,
(SELECT COUNT(*) FROM members WHERE ministerial='DAUGHTERS OF FAITH(DOF)')dof,
(SELECT COUNT(*) FROM members WHERE ministerial='EAGLES MINISTRY(SOF)')sof,
(SELECT COUNT(*) FROM members WHERE ministerial='JOSHUA CALEB MOMENTUM (JC)')jc,
(SELECT COUNT(*) FROM members WHERE ministerial='SUNDAY SCHOOL(SS)')ss
FROM DUAL"));
        $query2 = DB::select( DB::raw("SELECT
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE service='Sunday Service' AND  YEAR(created_at) = '$year')sunday,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE service='Prayer Service' AND YEAR(created_at) = '$year')prayer,
(SELECT IF(SUM(item) IS NULL,'50',SUM(item)) FROM offerings WHERE  service='Bible Study' AND YEAR(created_at) = '$year')bible
FROM DUAL
"));
        $query3s=Attenance::orderBy('id','desc')->where('service','Sunday Service')->take(8)->get();
        foreach ($query3s as $query3){
            $dates[]=$query3->date;
            $attendance[]=$query3->members;
        }
        $monthly=[
            $query[0]->january,
            $query[0]->february,
            $query[0]->march,
            $query[0]->april,
            $query[0]->may,
            $query[0]->june,
            $query[0]->july,
            $query[0]->augast,
            $query[0]->september,
            $query[0]->october,
            $query[0]->november,
            $query[0]->december,
        ];
        $service=[
            $query2[0]->sunday,
            $query2[0]->prayer,
            $query2[0]->bible,
        ];
        $gender=[
            $query[0]->dof,
            $query[0]->sof,
            $query[0]->jc,
            $query[0]->ss,
        ];
        return ['monthly'=>$monthly,'gender'=>$gender,'service'=>$service,'dates'=>$dates,'attendance'=>$attendance];
    }
}
