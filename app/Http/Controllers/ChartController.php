<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function pieChart(){
        $social_users=User::selectRaw('count(id) as count,id')->groupBy('id')->get();
        $user=array();
        foreach ($social_users as $result) {
            $user[$result->source]=(int)$result->count;
        }

//        return view('piechart',compact('user'));
        return $user;
    }

    public  function chartpage(){
        return view('piechart');
    }
}
