<?php

namespace App\Http\Controllers;

use App\Attenance;
use App\Category;
use App\Charts\UserChart;
use App\Member;
use App\Offering;
use App\Pledge;
use App\Pledge_data;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        $sum=Offering::whereYear('created_at', Carbon::now()->year)->sum('item');
        $tythes=Offering::where('offeringtype','Tithes')->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->sum('item');
        $missions=Offering::where('offeringtype','Missions')->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->sum('item');
        $thanksgiving=Offering::where('offeringtype','Thanks giving')->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->sum('item');
        $seed=Offering::where('offeringtype','Seed of Faith')->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->sum('item');
        $building=Offering::where('offeringtype','Building')->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->sum('item');
        $prophet=Offering::where('offeringtype','Prophets Honor')->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->sum('item');
        $freewill=Offering::where('offeringtype','Free Will Offering')->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->sum('item');
        $tv=Offering::where('offeringtype','Tv Support')->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->sum('item');
        $offering=Offering::where('offeringtype','Offering')->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->sum('item');
        $pledges=Pledge::whereYear('date', Carbon::now()->year)->sum('amount');
        $pledgesdata=Pledge_data::whereYear('date', Carbon::now()->year)->sum('amount');
        $user=Array(
            'Tithes'=>$tythes,
            'Missions'=>$missions,
            'Thanks giving'=>$thanksgiving,
            'Seed of Faith'=>$seed,
            'Building'=>$building,
            'Prophets Honor'=>$prophet,
            'Free Will'=>$freewill,
            'Tv Support'=>$tv,
            'Pledges'=>$pledges,
        );

        $members = Member::count();
        $zones = Category::count();
        $date = Carbon::now();
        $today = $date->format('d/m/Y');
        $offer = Offering::where('date', $today)->count();
        $count2=Attenance::count();
        if($count2<1){
            $count=0;
        }
        else{
            $count2=Attenance::latest()->first();
            $count=$count2->members;
        }
        $year=date("Y");
        return view('home', compact('user','members','zones','count','year','sum','pledges','pledgesdata'));
    }


    public function password()
    {
        return view('password');
    }

    public function postpassword(Request $request)
    {

        $this->validate($request, [
            'currentpass' => 'required|min:5',
            'newpass' => 'required|min:5',
            'repass' => 'required|min:5',
        ]);
        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return redirect()->back()->with('error', 'The current password is wrong');
        } elseif ($request->input('newpass') !== $request->input('repass')) {
            return redirect()->back()->with('error', 'The new passwords do not match');
        }
        $id = auth()->user()->id;
        $currentUser = User::findOrFail($id);
        $currentUser->password = Hash::make($request->input('newpass'));
        $currentUser->save();
        return redirect()->back()->with('success', 'Pasword changed successfully');

    }

    public function profile()
    {
        return view('profile');
    }

    public function  updateprofile(Request $request){
        $data=User::find(auth()->user()->id);
        $data->update($request->all());
        return redirect()->back()->with('success', 'Information updated successfully');


}

    public  function updateavarter(Request $request){
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
        ]);
        //get file name with extension
        $fileNameWithExt=$request->file('avatar')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('avatar')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore=$filename.'_'.time().'.'.$extesion;
        //uploadimage
        $path=$request->file('avatar')->storeAs('/public/avatars',$fileNameToStore);

        $user=User::find(Auth::user()->id);
        $user->avatars=$fileNameToStore;
        $user->save();

        return redirect()->back()->with('success','profile photo updated successfully');
    }
}
