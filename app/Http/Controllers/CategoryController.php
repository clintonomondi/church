<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

  public  function  postcategory(Request $request){
      $request->validate([
          'name' => 'required|unique:categories',
          'description' => 'required',
      ]);
      $cat=Category::create($request->all());
      return redirect()->route('categories')->with('success','Zone added successfully');
  }

  public  function  categories(){
      $cats=Category::all();
      return view('category.categories',compact('cats'));
  }


  public  function updatecat(Request $request,$id){
      $cat=Category::findorFail($id);
      $cat->update($request->all());
      return redirect()->route('categories')->with('success','Category updated successfully');
  }

  public  function getMembers($id){
      $datas=Category::find($id);
      return view('category.members',compact('datas'));
  }
}
