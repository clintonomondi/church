<?php

namespace App\Http\Controllers;

use App\Member;
use App\Offering;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OfferingController extends Controller
{
    public  function give(){
        $users=Member::all();
        return view('offering.giveoffering',compact('users'));
    }

    public  function postoffering(Request $request){

        $user =Member::find($request->member);
        if($user==null){
            return redirect()->back()->with('error','We could not get the user in the system');
        }
      $request['member_id']=$request->member;
        $request['category_id']=$user->category_id;
       $offer=Offering::create($request->all());
       return redirect()->back()->with('success','Offering submitted successfully');
    }
    public  function  offerings(){
        return view('offering.offerings');
    }

    public function searchofferings(Request $request){
        $from=$request->input('from');
        $to=$request->input('to');
        $toDate = date("d/m/Y", strtotime($to));
		 $fromDate = date("d/m/Y", strtotime($from));
        $offers=Offering::whereBetween('created_at', [$from, $to])->get();

        return view('offering.offerings',compact('offers','to','from'));
    }

    public  function editoffer($id){
        $users=Member::all();
        $offer=Offering::find($id);
        return view('offering.edit',compact('offer','users'));
    }
    public  function  updateoffer(Request $request,$id){
        $offer=Offering::find($id);
        $offer->update($request->all());
        return redirect()->route('offerings')->with('success','Offer updated successfully');
    }
}
