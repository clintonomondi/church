<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Member;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{

    public  function addmember(){
        $zones=Category::all();
        return view('member.addmember',compact('zones'));
    }

public  function postmember(Request $request){
   $ministerial='';
   $regno='';
   $data='';
   if($request->input('gender')=='Male'){
       $data='SOF';
       $count=Member::where('gender','Male')->count();
       if($count<1){
           $regno='1';
       }
       else {
           $regno = $count + 1;
       }
   }
   else if($request->input('gender')=='Female'){
       $data='DOF';
       $count=Member::where('gender','Female')->count();
       if($count<1){
           $regno='1';
       }
       else {
           $regno = $count + 1;
       }
   }
   else if($request->input('gender')=='Others'){
       $data='COMP';
       $count=Member::where('gender','Others')->count();
       if($count<1){
           $regno='1';
       }
       else {
           $regno = $count + 1;
       }
   }
    else{
        return redirect()->back()->with('error','Please select gender correctly');
    }

    $regno2=str_pad($regno,4,"0",STR_PAD_LEFT);
    $regno3=$data.''.$regno2;
    $request['regno']=$regno3;
    $member=Member::create($request->all());

    $member=Member::where('regno',$request->regno)->first();
    $comment=Member::find($member->id)->comment;
    $members=Member::all();
    $zones=Category::all();
    return view('member.searchresult',compact('member','comment','members','zones'));
}

public  function members(){
    $members=Member::all();
    return view('member.members',compact('members'));
}


public  function updatemember(Request $request,$id){
    $member=Member::find($id);
    $member->update($request->all());
    $zones=Category::all();
    $comment=Member::find($id)->comment;
    return view('member.searchresult',compact('member','comment','zones'));
}

public function search(){
    $zones=Category::all();
    return view('member.search',compact('zones'));
}

public  function  postSearch(Request $request){
    $member=Member::find($request->regno);
    if($member==null){
        return  redirect()->back()->with('error','Member not found in our system');
    }
    $comment=Member::find($request->regno)->comment;
    $zones=Category::all();
    return view('member.searchresult',compact('member','comment','zones'));
}

public  function  postComment(Request $request,$id){
    $validatedData = $request->validate([
        'comment' => 'required',
    ]);

    $request['member_id']=$id;
    $data=Comment::create($request->all());
    $member=Member::find($id);
    $zones=Category::all();
    $comment=Member::find($id)->comment;
    return view('member.searchresult',compact('member','comment','zones'));

    return redirect()->route('members')->with('success','Comment submitted successfully');
}

public  function  comments(){
    $comments=Comment::all();
    return view('member.comments',compact('comments'));
}

public  function memberview($id){
    $member=Member::find($id);
    return view('member.view',compact('member'));
}

public  function apendusers(Request $request){
    $q = $request->search;
    if($q == ''){
        $datas = Member::orderby('fname','asc')->select('fname','id')->limit(10)->get();;
    }else{
        $datas = Member::where('fname', 'LIKE', '%' . $q . '%')->orWhere('lname', 'LIKE', '%' . $q . '%')->orWhere('phone', 'LIKE', '%' . $q . '%')->orWhere('regno', 'LIKE', '%' . $q . '%')->get();
    }

    $response = array();
    foreach($datas as $data){
        $response[] = array(
            "id"=>$data->id,
            "text"=>$data->fname.' '.$data->lname
        );
    }
    return $response;
}


}
