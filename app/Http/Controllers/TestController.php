<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{

    public  function test(){
        $codes = DB::select( DB::raw("SELECT code FROM tests group by code") );
        return view('test',compact('codes'));
    }
    public function  getCountry(Request $request){
        $data=Test::where('code',$request->code)->get();

        return $data;
    }
}
