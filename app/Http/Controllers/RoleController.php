<?php

namespace App\Http\Controllers;

use App\Member;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RoleController extends Controller
{
    public  function users(){
        $users=User::all();
        $members=Member::all();
        return view('member.roles',compact('users','members'));
    }

    public  function postRole(Request $request){
        $member=Member::find($request->input('member_id'));
        if ($member->email==null){
            return redirect()->back()->with('error','email not found');
        }
        $request['name']=$member->fname;
        $request['email']=$member->email;
        $request['role']=$request->role;
        $user=User::create($request->all());
        $request['user_id']=$user->id;
        $member->update($request->all());
        return redirect()->back()->with('success','User added successfully');

    }

    public  function removeUser($id){
        $user=User::find($id);
        $user->delete();
        return redirect()->back()->with('success','User removed successfully');
    }

    public  function  postPassword(Request $request){
        $member = DB::table('members')->where('email', $request->input('email'))->first();
        if($member==null){
            return redirect()->back()->with('error','Member does not exist');
        }
        $user = DB::table('users')->where('id', $member->user_id)->first();
        if($user==null){
            return redirect()->back()->with('error','Member role has not be created,please contact system admin');
        }
        if($user->password!=null){
            return redirect()->back()->with('error','User had already create password,please login');
        }
        if ($request->input('repass') !== $request->input('password')) {
            return redirect()->back()->with('error', 'The new passwords are not matching');
        }

        $currentUser = User::findOrFail($user->id);
        $currentUser->password = Hash::make($request->input('password'));
        $currentUser->save();
        return redirect()->back()->with('success','Password created successfully,please login');

    }

    public function updateRole(Request $request){
        $data=User::find($request->member_id);
        $data->update($request->all());
        return redirect()->back()->with('success','Role updated successfully');
    }
}
