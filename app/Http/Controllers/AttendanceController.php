<?php

namespace App\Http\Controllers;

use App\Attenance;
use App\Offering;
use App\Summery;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AttendanceController extends Controller
{
    public function attendance(){
        $datas=Attenance::all();
   return view('attendance.attendance',compact('datas'));
    }

    public  function recordsummery(){
        $datas=Attenance::all();
        return view('attendance.record',compact('datas'));
    }

    public  function record2($id){
        $data=Attenance::find($id);
        return view('attendance.record2',compact('data'));
    }

    public  function postsummery(Request $request,$id){
        $request['attenance_id']=$id;
        $data=Summery::create($request->all());
        return redirect()->back()->with('success','Summery submitted successfully,you may add more summery');
    }

    public  function postattendance(Request $request){
        $date =$request->date;
        $today = date("d/m/Y", strtotime($date));
        $request['date']=$today;
        $offer=Attenance::create($request->all());
        $id=Attenance::select('id')->latest()->first();
        return redirect()->route('record2',$id)->with('success','Please provide summery');
    }

    public  function view($id){
    $datas=Attenance::find($id)->summery;
    return view('attendance.view',compact('datas'));
    }


    public  function searchsummery(Request $request){
        $from=$request->input('from');
        $to=$request->input('to');
        $toDate = date("d/m/Y", strtotime($to));
        $datas=Attenance::whereBetween('created_at', [$from, $to])->get();
        return view('attendance.attendance',compact('datas','to','from'));
    }
    

}
