<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pledge extends Model
{
    protected  $fillable=['member_id','amount','date','status','type'];

    public  function pledge_data(){
        return $this->hasMany(Pledge_data::class);
    }

    public  function member(){
        return $this->belongsTo(Member::class);
    }
}
