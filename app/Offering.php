<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offering extends Model
{
    protected  $fillable=['member_id','offeringtype','item','service','date','category_id'];

    public  function member(){
        return $this->belongsTo(Member::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
