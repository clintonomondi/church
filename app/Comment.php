<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=['comment','member_id'];

    public  function member(){
        return $this->belongsTo(Member::class);
    }
}
