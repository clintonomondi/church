<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attenance extends Model
{
    protected  $fillable=['service','members','date'];

    public  function summery(){
        return $this->hasMany(Summery::class);
    }
}
