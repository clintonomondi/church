var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$( document ).ready(function() {
    $('#process').hide();
    getData();
     // getUsers();
    //  postSearch();
    // submitPassword();
    // submitProfile();
    // submitMember();
});

function getData(){
    $.ajax({
        // url:"/getLineChartData",
        url:"http://192.168.3.11/church/public/getLineChartData",
        method:"GET",
        contentType:false,
        processData:false,
        catche:false,
        success:function(response)
        {
            var data2 = {
                labels: response.dates,
                datasets: [
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: response.attendance,
                    },
                ]
            };
            var data3 = {
                labels: ["Sunday Service", "Prayer Service", "Bible Study"],
                datasets: [
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: response.service,
                    },
                ]
            };
            var pdata = [
                {
                    value: response.gender[0],
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "DAUGHTERS OF FAITH(DOF)"
                },
                {
                    value: response.gender[1],
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "EAGLES MINISTRY(SOF)"
                },
                {
                    value: response.gender[2],
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "JOSHUA CALEB MOMENTUM (JC)"
                },
                {
                    value: response.gender[3],
                    color: "#3D64A0",
                    highlight: "#5CB85C",
                    label: "SUNDAY SCHOOL(SS)"
                }
            ]
            var ctxl = $("#lineChartDemo").get(0).getContext("2d");
            var lineChart = new Chart(ctxl).Line(data2);
            var ctxb = $("#barChartDemo").get(0).getContext("2d");
            var barChart = new Chart(ctxb).Bar(data3);
            var ctxp = $("#pieChartDemo").get(0).getContext("2d");
            var pieChart = new Chart(ctxp).Pie(pdata);
        }

    });
}
// function test(){
//     $( "#selUser" ).select2({
//         ajax: {
//             url:"/apendusers",
//             method:"GET",
//             contentType:false,
//             processData:false,
//             catche:false,
//             delay: 250,
//             data: function (params) {
//                 return {
//                     _token: CSRF_TOKEN,
//                     search: params.term // search term
//                 };
//             },
//             processResults: function (response) {
//                 return {
//                     results: response
//                 };
//             },
//             cache: true
//         }
// });
// }

// function postSearch() {
//     alert('am sslecetdd')
    $(document).on('change', '#languageselector', function (event) {
        var data;
        data = new FormData();
        data.append('code',$('#languageselector').val())
        $.ajax({
            url:"/getTest",
            method:"POST",
            processData: false,
            contentType: false,
            data:data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success:function(data) {
                var selOpts = "";
                $.each(data, function(k, v)
                {
                    var id = data[k].id;
                    var val = data[k].name;
                    selOpts += "<option value='"+id+"'>"+val+"</option>";
                });
                $('#places') .empty().append(selOpts);
            },
            error: function(data) {
                alertify.error('Internal server error');
            }
        });
    });
// }
//
// function submitMember(){
//     $(document).on('submit', '#submitMember', function(event){
//         event.preventDefault();
//         $('#myModal2').modal('show');
//         var data;
//         data = new FormData(this);
//         $.ajax({
//             url:"/members",
//             method:"POST",
//             data: data,
//             contentType:false,
//             processData:false,
//             success:function(data)
//             {
//                 if(data.status)
//                 {
//                     $('#myModal2').modal('hide');
//                     $.notify(data.message, "success");
//                     $(':input','#submitMember')
//                         .not(':button,:submit,:hidden')
//                         .val('')
//                         .prop('checked',false)
//                         .prop('selected',false);
//
//                 }
//             },
//             error:function(data)
//             {
//                 if(data.status)
//                 {
//                     $('#myModal2').modal('hide');
//                     $.notify('The data you submitted is invalid', "error");
//                 }
//             }
//
//         });
//     });
// }
//
// function submitPassword() {
//     $(document).on('submit', '#submitPassword', function(event){
//         event.preventDefault();
//         $('#myModals2').modal('show');
//         var data;
//         data = new FormData(this);
//         $.ajax({
//             url:"/password",
//             method:"POST",
//             data: data,
//             contentType:false,
//             processData:false,
//             success:function(data)
//             {
//                 if(data.status)
//                 {
//                     $('#myModals2').modal('hide');
//                     $.notify(data.message, "success");
//                     $(':input','#submitPassword')
//                         .not(':button,:submit,:hidden')
//                         .val('')
//                         .prop('checked',false)
//                         .prop('selected',false);
//                 }
//                 else
//                 {
//                     $('#myModals2').modal('hide');
//                         $.notify(data.message, "error");
//                 }
//             },
//             error:function(data)
//             {
//                 if(data.status)
//                 {
//                     $('#myModals2').modal('hide');
//                     $.notify('Invalid data or Server error', "error");
//                 }
//             }
//         });
//     });
// }
//
// function submitProfile() {
//     $(document).on('submit', '#submitProfile', function(event){
//         event.preventDefault();
//         $('#myModals2').modal('show');
//         var data;
//         data = new FormData(this);
//         $.ajax({
//             url:"/profile/submit",
//             method:"POST",
//             data: data,
//             contentType:false,
//             processData:false,
//             success:function(data)
//             {
//                 if(data.status)
//                 {
//                     $('#myModals2').modal('hide');
//                     $.notify(data.message, "success");
//                 }
//                 else
//                 {
//                     $('#myModals2').modal('hide');
//                     $.notify(data.message, "error");
//                 }
//             },
//             error:function(data)
//             {
//                 if(data.status)
//                 {
//                     console.log(data.message);
//                     $.notify('Invalid data or Server error', "error");
//                     $('#myModals2').modal('hide');
//                 }
//             }
//         });
//     });
// }
//
// function Submit() {
//         $('#myModals2').modal('show');
//
// }
//
//
//
//
