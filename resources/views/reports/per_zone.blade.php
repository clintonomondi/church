@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="tile">
                <div>
                    @if(empty($to))
                        <p id="dataname">All Zones Report</p>
                    @else
                        <p id="dataname">Giving Report for all zones from {{$from}} to {{$to}} </p>
                    @endif
                </div>
                <div class="tile-body">
                    <form method="post" action="{{route('post_per_zone')}}">
                        <div class="row justify-content-center">
                            @csrf
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Period From</label>
                                    <input class="form-control" id="demoDate" type="date" placeholder="Select Date from" name="from">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Period To</label>
                                    <input class="form-control"  type="date" placeholder="Select Date to" name="to">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-search"></i>Search</button>&nbsp;
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                {{--                <h3 class="tile-title">Vertical Form</h3>--}}
                <div class="tile-body">
                    @include('includes.message')
                    <table class="table table-hover table-bordered table-striped" id="table">
                        <thead>
                        <th>#</th>
                        <th>Zone</th>
                        <th>Actual Members</th>
                        <th>Members Contributed</th>
                        <th>Amount (KSh.)</th>
                        <th>Tithes</th>
                        <th>TV support</th>
                        <th>Free Will Offering</th>
                        </thead>
                        <tbody>
                        @foreach($datas as $key=>$data)
                            @if(empty($to))
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data->member->count()}}</td>
                                <td>{{$data->offering->groupBy('member_id')->count()}}</td>
                                <td>{{number_format($data->offering->sum('item'),2)}}</td>
                                <td>{{number_format($data->offering->where('offeringtype','Tithes')->sum('item'),2)}}</td>
                                <td>{{number_format($data->offering->where('offeringtype','Tv Support')->sum('item'),2)}}</td>
                                <td>{{number_format($data->offering->where('offeringtype','Free Will Offering')->sum('item'),2)}}</td>
                            </tr>
                                @else
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->member->count()}}</td>
                                    <td>{{$data->offering->whereBetween('created_at', [$from, $to])->count()}}</td>
                                    <td>{{number_format($data->offering->whereBetween('created_at', [$from, $to])->sum('item'),2)}}</td>
                                    <td>{{number_format($data->offering->whereBetween('created_at', [$from, $to])->where('offeringtype','Tithes')->sum('item'),2)}}</td>
                                    <td>{{number_format($data->offering->whereBetween('created_at', [$from, $to])->where('offeringtype','Tv Support')->sum('item'),2)}}</td>
                                    <td>{{number_format($data->offering->whereBetween('created_at', [$from, $to])->where('offeringtype','Free Will Offering')->sum('item'),2)}}</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

@endsection
