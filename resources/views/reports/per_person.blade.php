@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="tile">
                <div>
                    @if(empty($member->fname))
                    @else
                        <p id="dataname">Giving Report for {{$member->fname}}    {{$member->lname}} from {{$from}} to {{$to}}</p>
                    @endif
                </div>
                <div class="tile-body">
                    <form method="post" action="{{route('post_per_person')}}">
                  <div class="row justify-content-center">
                          @csrf
                      <div class="col-sm-3">
                              <div class="form-group">
                                  <label for="sel1">Giver:</label>
                                  <select class="form-control" id="selUser" name="member_id">
                                      <option value='0'>-- Select user --</option>
                                  </select>
                              </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group">
                              <label class="control-label">Period From</label>
                              <input class="form-control" id="demoDate" type="date" placeholder="Select Date from" name="from">
                          </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group">
                              <label class="control-label">Period To</label>
                              <input class="form-control"  type="date" placeholder="Select Date to" name="to">
                          </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group">
                          <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-search"></i>Search</button>&nbsp;
                          </div>
                      </div>
                  </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                {{--                <h3 class="tile-title">Vertical Form</h3>--}}
                <div class="tile-body">
                    @include('includes.message')
                    <table class="table table-bordered table-striped display nowrap" id="table">
                        <thead>
                        <th>#</th>
                        <th>Amount(Ksh.)</th>
                        <th>Offering Type</th>
                        <th>Service</th>
                        <th>Phone No</th>
                        <th>Date</th>
                        </thead>
                        <tbody>
                @foreach($datas as $key=>$data)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{number_format($data->item,2)}}</td>
                        <td>{{$data->offeringtype}}</td>
                        <td>{{$data->service}}</td>
                        <td>{{$data->member->phone}}</td>
                        <td>{{$data->date}}</td>
                    </tr>

                    @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

@endsection
