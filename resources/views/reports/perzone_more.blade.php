@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <p id="dataname">Giving Report for {{$zone->name}}   {{$from}}   to    {{$to}} </p>
                <div class="tile-body">
                    @include('includes.message')
                    <table class="table table-hover table-bordered table-striped" id="table">
                        <thead>
                        <th>#</th>
                        <th>Offering Type.</th>
                        <th> Sum Amount(Ksh.)</th>
                        </thead>
                        <tbody>
                        @foreach($datas as $key=>$data)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$data->offeringtype}}</td>
                                <td>{{$data->sum}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

@endsection
