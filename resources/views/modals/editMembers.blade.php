<!-- The Modal -->
<div class="modal fade" id="{{$member->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Edit member</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post" action="{{route('updatemember',$member->id)}}">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <div class="tile">
                                {{--                <h3 class="tile-title">Vertical Form</h3>--}}
                                <div class="tile-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">First Name</label>
                                                        <input class="form-control" value="{{$member->fname}}" name="fname" type="text" placeholder="Enter first name" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Last Name</label>
                                                        <input class="form-control" value="{{$member->lname}}" name="lname" type="text" placeholder="Enter last name" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Email address</label>
                                                        <input class="form-control" value="{{$member->email}}" name="email" type="text" placeholder="Enter email">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="sel1">Marital status:</label>
                                                        <select class="form-control" id="sel1" name="marital">
                                                            <option value="{{$member->marital}}">{{$member->marital}}</option>
                                                            <option>Single</option>
                                                            <option>Married</option>
                                                            <option>Divorced</option>
                                                            <option>Widowed</option>
                                                            <option>Separated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Date of birth</label>
                                                        <input class="form-control" value="{{$member->dob}}"   type="text" placeholder="Select Date" name="dob">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Phone Number</label>
                                                        <input class="form-control" value="{{$member->phone}}" name="phone" type="text" placeholder="Enter phone no">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="sel1">Gender:</label>
                                                        <select class="form-control" id="sel1" name="gender">
                                                            <option value="{{$member->gender}}">{{$member->gender}}</option>
                                                            <option >Male</option>
                                                            <option>Female</option>
                                                            <option>Others</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="sel1">Area of ministry:</label>
                                                        <select class="form-control" id="sel1" name="ministry">
                                                            <option value="{{$member->ministry}}">{{$member->ministry}}</option>
                                                            <option>Usher</option>
                                                            <option>Security</option>
                                                            <option>Intercessor</option>
                                                            <option>Choir</option>
                                                            <option>Protocol</option>
                                                            <option>Media</option>
                                                            <option>Zone Pastor</option>
                                                            <option>Ass. Zone Pastor </option>
                                                            <option>Home Churh Pastor </option>
                                                            <option>Ass. Home Churh Pastor </option>
                                                            <option>Evangelism </option>
                                                            <option>Catering </option>
                                                            <option>Pastoral </option>
                                                            <option>Sunday Sch. Teacher </option>
                                                            <option>None </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><hr>
                                        {{--<div id="menu1" class="container tab-pane fade"><br>--}}







                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="sel1">Born Again?:</label>
                                                        <select class="form-control" id="sel1" name="born">
                                                            <option>{{$member->born}}</option>
                                                            <option>Yes</option>
                                                            <option>No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Residence</label>
                                                        <input class="form-control" value="{{$member->residence}}" name="residence" type="text" placeholder="Enter residence">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Home Church</label>
                                                        <input class="form-control" value="{{$member->church}}" name="church" type="text" placeholder="Enter home church">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="sel1">Zone:</label>
                                                        <select class="form-control" id="sel1" name="category_id" required>
                                                            <option value="{{$member->category->id}}">{{$member->category->name}}</option>
                                                            @foreach($zones as $zone)
                                                                <option value="{{$zone->id}}">{{$zone->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Parent Name(For sunday School)</label>
                                                        <input class="form-control" value="{{$member->parentName}}" name="parentName" type="text" placeholder="Enter parent name">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Parent phone no(For sunday school)</label>
                                                        <input class="form-control" value="{{$member->parentPhone}}" name="parentPhone" type="text" placeholder="Enter parent phone">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Spouse</label>
                                                        <input class="form-control" value="{{$member->spouse}}" name="spouse" type="text" placeholder="Enter parent phone">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Ministerial Team</label>
                                                        <select class="form-control" id="sel1" name="ministerial">
                                                            <option>{{$member->ministerial}}</option>
                                                            <option>DAUGHTERS OF FAITH(DOF)</option>
                                                            <option>EAGLES MINISTRY(SOF)</option>
                                                            <option>JOSHUA CALEB MOMENTUM (JC)</option>
                                                            <option>SUNDAY SCHOOL(SS)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tile-footer">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;
                                            </div>
                                        {{--</div>--}}
                                    </div>
                                </div>


                            {{--</div>--}}
                        {{--</div>--}}
                    </div>

                </form>

            </div>

        </div>
    </div>
</div>
