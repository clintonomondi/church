<!-- The Modal -->
<div class="modal fade" id="password">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Create Password</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post" id="submitCategory" action="{{route('postPassword')}}">
                    @csrf
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input class="form-control" name="email" type="email" placeholder="Enter email" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Password</label>
                        <input class="form-control" name="password" type="password" placeholder="Enter password" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Confirm password</label>
                        <input class="form-control" name="repass" type="password" placeholder="Re-enter password" required>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>&nbsp;&nbsp;&nbsp;
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
