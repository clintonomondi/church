<!-- The Modal -->
<div class="modal fade" role="dialog" id="{{$data->id}}">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Pledge Payment</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <ul>
                    <li>{{$data->member->fname}}   {{$data->member->lname}}</li>
                    <li>{{$data->member->regno}}</li>
                    <li>{{$data->member->phone}}</li>
                </ul><hr>
                <form method="post"  action="{{route('pay',$data->id)}}">
                    @csrf
                    <div class="form-group">
                        <label class="control-label">Amount</label>
                        <input class="form-control" name="amount" type="number" placeholder="Enter amount to pay" required>
                    </div>
                        <div class="form-group">
                            <label class="control-label">Date</label>
                            <input class="form-control" id="demoDate" type="date" placeholder="Select Date from" name="date" required>
                        </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Proceed</button>&nbsp;
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
