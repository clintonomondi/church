<!-- The Modal -->
<div class="modal fade" id="{{$cat->id}}">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Edit Zone</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post" action="{{route('updatecat',$cat->id)}}">
                    @csrf
                    <div class="form-group">
                        <label class="control-label">Category Name</label>
                        <input class="form-control" name="name" type="text"value="{{$cat->name}}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Zonal Paster</label>
                        <input class="form-control" name="paster" type="text"value="{{$cat->paster}}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Description</label>
                        <textarea class="form-control" rows="5" name="description" placeholder="Enter your address">{{$cat->description}}</textarea>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
                    </div>
                </form>

            </div>


        </div>
    </div>
</div>
