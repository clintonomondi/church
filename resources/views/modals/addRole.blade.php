<!-- The Modal -->
<div class="modal fade" role="dialog" id="addroles">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Users</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post"  action="{{route('postRole')}}">
                    @csrf
                    <div class="form-group">
                        <label for="sel1">Users:</label>
                        <select class="form-control" id="selUser" name="member_id">
                            <option value='0'>-- Select user --</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sel1">Roles:</label>
                        <select class="form-control" id="sel1" name="role">
                            <option disabled="disabled" selected="selected">Select role..</option>
                            <option>admin</option>
                            <option>dataoperator</option>
                            <option>query</option>
                        </select>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>&nbsp;
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
