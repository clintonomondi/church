<!-- The Modal -->
<div class="modal fade" id="processModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Please wait...</h4>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="spinner-border text-success"></div>
                </div>
            </div>

        </div>
    </div>
</div>
