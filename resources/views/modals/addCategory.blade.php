<!-- The Modal -->
<div class="modal fade" id="addZone">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Zones</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post" id="submitCategory" action="{{route('postcategory')}}">
                    @csrf
                    <div class="form-group">
                        <label class="control-label">Zone Name</label>
                        <input class="form-control" name="name" type="text" placeholder="Enter full name">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Zonal Paster</label>
                        <input class="form-control" name="paster" type="text" placeholder="Enter name of paster">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Area Covered</label>
                        <textarea class="form-control" rows="5" name="description" placeholder="Enter your address"></textarea>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
