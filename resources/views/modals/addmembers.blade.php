<!-- The Modal -->
<div class="modal fade" id="addMembers">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Members</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post" action="{{route('postmember')}}">
                    @csrf
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="tile">
                            {{--                <h3 class="tile-title">Vertical Form</h3>--}}
                            <div class="tile-body">
                            <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home">STEP ONE</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#menu1">STEP TWO</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div id="home" class="container tab-pane active"><br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">First Name</label>
                                                    <input class="form-control" name="fname" type="text" placeholder="Enter first name" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Last Name</label>
                                                    <input class="form-control" name="lname" type="text" placeholder="Enter last name" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Email address</label>
                                                    <input class="form-control" name="email" type="text" placeholder="Enter email">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sel1">Marital status:</label>
                                                    <select class="form-control" id="sel1" name="marital">
                                                        <option disabled="disabled" selected="selected">Select marital status</option>
                                                        <option>Single</option>
                                                        <option>Married</option>
                                                        <option>Divorced</option>
                                                        <option>Widowed</option>
                                                        <option>Separated</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Date of birth</label>
                                                    <input class="form-control"  type="date" placeholder="Select Date" name="dob">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Phone Number</label>
                                                    <input class="form-control" name="phone" type="text" placeholder="Enter phone no">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sel1">Gender:</label>
                                                    <select class="form-control" id="sel1" name="gender">
                                                        <option disabled="disabled" selected="selected">Select gender..</option>
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                        <option>Others</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sel1">Area of ministry:</label>
                                                    <select class="form-control" id="sel1" name="ministry">
                                                        <option disabled="disabled" selected="selected">Select area of ministry..</option>
                                                        <option>Usher</option>
                                                        <option>Security</option>
                                                        <option>Intercessor</option>
                                                        <option>Choir</option>
                                                        <option>Protocol</option>
                                                        <option>Media</option>
                                                        <option>Zone Pastor</option>
                                                        <option>Ass. Zone Pastor </option>
                                                        <option>Home Churh Pastor </option>
                                                        <option>Ass. Home Churh Pastor </option>
                                                        <option>Evangelism </option>
                                                        <option>Catering </option>
                                                        <option>Pastoral </option>
                                                        <option>Sunday Sch. Teacher </option>
                                                        <option>None </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr>
                                    <div id="menu1" class="container tab-pane fade"><br>







                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sel1">Born Again?:</label>
                                                    <select class="form-control" id="sel1" name="born">
                                                        <option disabled="disabled" selected="selected"> Select ...</option>
                                                        <option>Yes</option>
                                                        <option>No</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Residence</label>
                                                    <input class="form-control" name="residence" type="text" placeholder="Enter residence">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Home Church</label>
                                                    <input class="form-control" name="church" type="text" placeholder="Enter home church">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sel1">Zone:</label>
                                                    <select class="form-control" id="zone" name="category_id" required>
                                                        <option disabled="disabled" selected="selected">Select Zone</option>
                                                        @foreach($zones as $zone)>
                                                        <option value="{{$zone->id}}">{{$zone->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Parent Name(For sunday School)</label>
                                                    <input class="form-control" name="parentName" type="text" placeholder="Enter parent name">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Parent phone no(For sunday school)</label>
                                                    <input class="form-control" name="parentPhone" type="text" placeholder="Enter parent phone">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Spouse Name</label>
                                                    <input class="form-control" name="spouse" type="text" placeholder="Enter spouse name">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sel1">Ministerial team:</label>
                                                    <select class="form-control" id="sel1" name="ministerial">
                                                        <option disabled="disabled" selected="selected"> Select ministerial team</option>
                                                        <option>DAUGHTERS OF FAITH(DOF)</option>
                                                        <option>EAGLES MINISTRY(SOF)</option>
                                                        <option>JOSHUA CALEB MOMENTUM (JC)</option>
                                                        <option>SUNDAY SCHOOL(SS)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tile-footer">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                </form>

            </div>

        </div>
    </div>
</div>
