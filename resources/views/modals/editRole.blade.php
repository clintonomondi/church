<!-- The Modal -->
<div class="modal fade" id="{{$user->id}}">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{$user->name}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post"  action="{{route('updateRole')}}">
                    @csrf
                    <input class="form-control" value="{{$user->id}}" name="member_id" type="text"  hidden required>
                    <div class="form-group">
                        <label for="sel1">Roles:</label>
                        <select class="form-control" id="sel1" name="role">
                            <option >{{$user->role}}</option>
                            <option>admin</option>
                            <option>dataoperator</option>
                            <option>query</option>
                        </select>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
