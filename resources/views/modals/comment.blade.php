<!-- The Modal -->
<div class="modal fade" id="comment">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Comment</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post" id="submitCategory" action="{{route('postComment',$member->id)}}">
                    @csrf
                    <div class="form-group">
                        <label class="control-label">Comment</label>
                        <textarea class="form-control" rows="5" name="comment" placeholder="Comment"></textarea>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>
