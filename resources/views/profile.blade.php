@extends('layouts.app')

@section('content')
    @include('includes.message')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="tile">
            <h6 class="tile-title">Edit Profile Logo</h6>
            <div class="tile-body">


        <div class="img bg-wrap text-center py-4" style="background-image: url({{asset('sidebar/images/bg.jpeg')}});">
            <a class="user-logo" href="{{route('profile')}}">
                @if(auth()->user()->avatars==null)
                    <div class="img" style="background-image: url({{asset('images/user.png')}});"></div>
                @else
                    <div class="img" style="background-image: url('/storage/avatars/{{ Auth::user()->avatars }}');"></div>
                @endif
                <h3>{{auth()->user()->name}}</h3>
            </a>
        </div><hr>
                <form action="{{route('updateavarter')}}" method="post" enctype="multipart/form-data">
                    @csrf
    <div class="form-group">
        <label class="control-label">Uplaod new image</label>
        <input class="form-control" name="avatar"   type="file" placeholder="select a photo">
    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Upload</button>
                    </div>

                </form>

            </div>
        </div>
        </div>

        <div class="col-md-6">
            <div class="tile">
                <h6 class="tile-title">Edit Profile</h6>
                <div class="tile-body">
                <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu1">Password</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="home" class="container tab-pane active"><br>
                            <form method="post" action="{{route('updateprofile')}}">
                                @csrf
                                <div class="form-group">
                                    <label class="control-label">Role</label>
                                    <input class="form-control" value="{{Auth::user()->role}}" name="role" type="text" placeholder="role" readonly>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input class="form-control" value="{{Auth::user()->name}}" name="name" type="text" placeholder="Enter full name">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input class="form-control" name="email" value="{{Auth::user()->email}}"  type="email" placeholder="Enter full name">
                                </div>

                                <div class="tile-footer">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
                                </div>
                            </form>
                        </div>
                        <div id="menu1" class="container tab-pane fade"><br>
                            <form method="post" action="{{route('postpassword')}}">
                                @csrf
                                <div class="form-group">
                                    <label class="control-label">Current password</label>
                                    <input class="form-control" name="currentpass" type="password" placeholder="Enter current password">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">New password</label>
                                    <input class="form-control" name="newpass" type="password" placeholder="Enter new password">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Confirm password</label>
                                    <input class="form-control" name="repass" type="password" placeholder="Enter re eter password">
                                </div>

                                <div class="tile-footer">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
                                </div>
                            </form>

                        </div>
                </div>

            </div>
        </div>
    </div>


@endsection
