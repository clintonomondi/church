@extends('layouts.app')

@section('content')
    <div class="app-title">
        <div>
            <p>Step 2</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{route('attendance')}}">Back to Summery</a></li>
        </ul>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row justify-content-center">
                        <div class="col-sm-4">
                            <h2>Service</h2>{{$data->service}}
                        </div>
                        <div class="col-sm-4">
                            <h2>Members</h2>{{$data->members}}
                        </div>
                        <div class="col-sm-4">
                            <h2>Date</h2>{{$data->date}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <p class="tile-title">Summary</p>
                <div class="tile-body">
<p>Please provide records </p>
                    @include('includes.message')
                    <form method="post" action="{{route('postsummery',$data->id)}}">
                    <div class="row justify-content-center">
                    @csrf
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="sel1">Offering Type:</label>
                            <select class="form-control" id="sel1" name="giving" required>
                                <option>Tithes</option>
                                <option>Thanks giving</option>
                                <option>Seed of Faith</option>
                                <option>Missions</option>
                                <option>Building </option>
                                <option>Prophets Honor </option>
                                <option>Free Will Offering </option>
                                <option>Offering </option>
                                <option>Tv Support </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                    <div class="form-group">
                    <label class="control-label">Total Amount</label>
                    <input class="form-control"  type="number"  name="amount" required>
                    </div>
                    </div>

                    <div class="form-group">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-arrow-right"></i>Submit</button>&nbsp;

                    </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
@endsection

