@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <div>
                    <p id="dataname">Summery</p>
                </div>
                <div class="tile-body">
                    <table class="table table-bordered table-striped display nowrap" id="table">
                        <thead>
                        <th>#</th>
                        <th>Service</th>
                        <th>Attendance</th>
                        <th>Type of Giving</th>
                        <th>Amount(Ksh.)</th>
                        <th>date</th>
                        </thead>
                        <tbody>
                        @foreach($datas as  $key=>$data)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$data->attenance->service}}</td>
                                <td>{{$data->attenance->members}}</td>
                                <td>{{$data->giving}}</td>
                                <td>{{number_format($data->amount,2)}}</td>
                                <td>{{$data->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection

