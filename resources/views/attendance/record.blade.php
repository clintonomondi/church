@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            @include('includes.message')
            <div class="tile">
                <h6>Attendance</h6>
                <div class="tile-body">
                    <form method="post" action="{{route('postattendance')}}">
                        <div class="row justify-content-center">
                            @csrf
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="sel1">Service:</label>
                                    <select class="form-control" id="sel1" name="service" required>
                                        <option>Prayer Service</option>
                                        <option>Bible study</option>
                                        <option>Sunday Service</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Number of members</label>
                                    <input class="form-control"  type="number"  name="members" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Date</label>
                                    <input class="form-control"  type="date"  name="date" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-arrow-right"></i>Next</button>&nbsp;
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <div>
                    <p id="dataname">All Summery</p>
                </div>
                <div class="tile-body">
                    <table class="table table-bordered table-striped display nowrap" id="table">
                        <thead>
                        <th>#</th>
                        <th>Service</th>
                        <th>Date</th>
                        <th>Attendance</th>
                        <th></th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach($datas as  $key=>$data)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$data->service}}</td>
                                <td>{{$data->date}}</td>
                                <td>{{$data->members}}</td>
                                <td><a class="fa fa-eye" href="{{route('attendance.view',$data->id)}}" >View summery</a> </td>
                                <td><a class="fa fa-edit" href="{{route('record2',$data->id)}}"></a> </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection

