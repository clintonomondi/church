@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            @include('includes.message')
            <div class="tile">

                <form method="post" action="{{route('searchsummery')}}">
                    <div class="row justify-content-center">
                        @csrf
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Period From</label>
                                <input class="form-control" id="demoDate" type="date" placeholder="Select Date from" name="from">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Period To</label>
                                <input class="form-control"  type="date" placeholder="Select Date to" name="to">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-search"></i>Search</button>&nbsp;
                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <div>
                    @if(empty($to))
                    <p id="dataname">Attendance Report</p>
                    @else
                        <p id="dataname">Attendance report from {{$from}} to {{$to}}</p>
                    @endif
                </div>
                <div class="tile-body">
                    <table class="table table-bordered table-striped display nowrap" id="table">
                        <thead>
                        <th>#</th>
                        <th>Service</th>
                        <th>Date</th>
                        <th>Attendance</th>
                        <th>Tithes(Ksh.)</th>
                        <th>TV support(Ksh.)</th>
                        <th>Free Will Offering(Ksh.)</th>
                        <th></th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach($datas as  $key=>$data)
                            @if(empty($to))
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$data->service}}</td>
                                <td>{{$data->date}}</td>
                                <td>{{$data->members}}</td>
                                <td>{{number_format($data->summery->where('giving','Tithes')->sum('amount'),2)}}</td>
                                <td>{{number_format($data->summery->where('giving','Tv Support')->sum('amount'),2)}}</td>
                                <td>{{number_format($data->summery->where('giving','Free Will Offering')->sum('amount'),2)}}</td>
                                <td><a class="fa fa-eye btn btn-info btn-sm" href="{{route('attendance.view',$data->id)}}" ></a> </td>
                                <td><a class="fa fa-edit btn btn-primary btn-sm" href="{{route('record2',$data->id)}}"></a> </td>
                            </tr>
                            @else
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$data->service}}</td>
                                    <td>{{$data->date}}</td>
                                    <td>{{$data->members}}</td>
                                    <td>{{number_format($data->summery->whereBetween('created_at', [$from, $to])->where('giving','Tithes')->sum('amount'),2)}}</td>
                                    <td>{{number_format($data->summery->whereBetween('created_at', [$from, $to])->where('giving','Tv Support')->sum('amount'),2)}}</td>
                                    <td>{{number_format($data->summery->whereBetween('created_at', [$from, $to])->where('giving','Free Will Offering')->sum('amount'),2)}}</td>
                                    <td><a class="fa fa-eye btn btn-info btn-sm" href="{{route('attendance.view',$data->id)}}" ></a> </td>
                                    <td><a class="fa fa-edit btn btn-primary btn-sm" href="{{route('record2',$data->id)}}"></a> </td>
                                </tr>
                                @endif
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection

