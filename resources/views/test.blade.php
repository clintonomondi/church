@extends('layouts.app')

@section('content')
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-12">--}}
{{--            <div class="tile">--}}
{{--                <h6 class="tile-title">Edit Profile</h6>--}}
{{--                <div class="tile-body">--}}
{{--                @include('includes.message')--}}
{{--                <!-- Nav tabs -->--}}
{{--                    <ul class="nav nav-tabs nav-justified" role="tablist">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link active" data-toggle="tab" href="#home">Profile</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" data-toggle="tab" href="#menu1">Password</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                    <div class="tab-content">--}}
{{--                        <div id="home" class="container tab-pane active"><br>--}}


{{--                                <div class="col-sm-6">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="sel1">Select Code:</label>--}}
{{--                                        <select class="form-control" id="languageselector" name="languageselector">--}}
{{--                                            <option disabled="disabled" selected="selected">Select code</option>--}}
{{--                                            @foreach($codes as $code)--}}
{{--                                                <option value="{{$code->code}}">{{$code->code}}</option>--}}
{{--                                                @endforeach--}}


{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="col-sm-6">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="sel1">Places names:</label>--}}
{{--                                        <select class="form-control" id="places" name="marital">--}}

{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}


{{--                        </div>--}}
{{--                        <div id="menu1" class="container tab-pane fade"><br>--}}
{{--                            <form method="post" action="{{route('postpassword')}}">--}}
{{--                                @csrf--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="control-label">Current password</label>--}}
{{--                                    <input class="form-control" name="currentpass" type="password" placeholder="Enter current password">--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="control-label">New password</label>--}}
{{--                                    <input class="form-control" name="newpass" type="password" placeholder="Enter new password">--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="control-label">Confirm password</label>--}}
{{--                                    <input class="form-control" name="repass" type="password" placeholder="Enter re eter password">--}}
{{--                                </div>--}}

{{--                                <div class="tile-footer">--}}
{{--                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>--}}
{{--                                </div>--}}
{{--                            </form>--}}

{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}



        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="tile">
                    <h6 class="tile-title">PAYROL</h6>
                    <div class="tile-body">
                           <div class="form-group">
                               <label class="control-label">Basic Salary</label>
                               <input class="form-control" name="repass" type="number">
                           </div>
                        <div class="row">
                            <div class="col-sm-6">
                           <h6 style=" color: red;"><u>ALLOWANCES</u></h6>
                           <div class="form-group">
                               <label class="control-label">House Allowance</label>
                               <input class="form-control" name="repass" type="number">
                           </div>
                           <div class="form-group">
                               <label class="control-label">Dog Allowance</label>
                               <input class="form-control" name="repass" type="number">
                           </div>
                       </div>
                            <div class="col-sm-6">
                                <h6 style=" color: red;"><u>DEDUCTIONS</u></h6>
                                <div class="form-group">
                                    <label class="control-label">NHIF</label>
                                    <input class="form-control" name="repass" type="number">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">PAYE</label>
                                    <input class="form-control" name="repass" type="number">
                                </div>
                    </div>
                            <button class="btn btn-success float-right">CALCULATE</button></div>
                            <button class="btn btn-primary float-right fa fa-print">Print</button></div><hr>

                            <table class="table table-hover table-bordered" id="table" >
                                <thead>
                                <th>Resource</th>
                                <th>Value</th>
                                </thead>
                                <tbody>
<tr>
    <td>Basic Salary</td>
    <td>00</td>
</tr>
<tr>
    <td>Total Allowances</td>
    <td>00</td>
</tr>
<tr>
    <td>Total Deductions</td>
    <td>00</td>
</tr>
<tr>
    <td>Taxable Income</td>
    <td>00</td>
</tr>
<tr style="color: blue;">
    <td>Net Income</td>
    <td>00</td>
</tr>
                                </tbody>

                            </table>
                </div>
            </div>
            </div>
        </div>

@endsection
