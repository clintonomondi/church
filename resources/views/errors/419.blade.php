<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Life Celebration center</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Main CSS-->
    <script src="{{asset('docs/js/jquery-3.2.1.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('docs/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/file.css')}}">
    <!-- Font-icon css-->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css"></script>

    <link href="{{asset('select2/dist/css/select2.min.css')}}" rel="stylesheet" />
    <script src="{{asset('select2/dist/js/select2.min.js')}}"></script>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('sidebar/css/style.css')}}">
    <script src="{{asset('js/sweetalert.min.js')}}"></script>
</head>
<body>


<main class="app-content">
    <div class="page-error tile">
        <div class="row justify-content-center">
            <img src="{{asset('images/logo.jpeg')}}" class="center" style=" border-radius: 50%;">
        </div>
        <h1><i class="fa fa-exclamation-circle"></i> Error 419: Page Has expired</h1>
        <p>The page you have requested token has expired.</p>
        <p><a class="btn btn-primary" href="{{route('login')}}">Home</a></p>
    </div>
</main>


{{--<script src="{{asset('sidebar/js/jquery.min.js')}}"></script>--}}
<script src="{{asset('sidebar/js/popper.js')}}"></script>
{{--<script src="sidebar/js/bootstrap.min.js"></script>--}}
<script src="{{asset('sidenar/js/main.js')}}"></script>

<script src="{{asset('js/api.js')}}"></script>
<script src="{{asset('js/task.js')}}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {

        var record={!! json_encode($user) !!};
        console.log(record);
        // Create our data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'source');
        data.addColumn('number', 'Total_Signup');
        for(var k in record){
            var v = record[k];

            data.addRow([k,v]);
            console.log(v);
        }
        var options = {
            title: '',
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
    }

    google.visualization.events.addListener(chart, 'select', function() {

        var row = chart.getSelection()[0].row;
        var selected_data=data.getValue(row, 0);
        var url = "demo.expertphp/google-pie-chart?data="+selected_data ;
        window.open(url, '_blank');
    });

</script>
<!-- Essential javascripts for application to work-->
<script src="{{asset('docs/js/popper.min.js')}}"></script>
<script src="{{asset('docs/js/bootstrap.min.js')}}"></script>
<script src="{{asset('docs/js/main.js')}}"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="{{asset('docs/js/plugins/pace.min.js')}}"></script>

<!-- Page specific javascripts-->
<script type="text/javascript" src="{{asset('docs/js/plugins/chart.js')}}"></script>
<!-- Essential javascripts for application to work-->

<!-- Google analytics script-->
<script type="text/javascript">
    if(document.location.hostname == 'pratikborsadiya.in') {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-72504830-1', 'auto');
        ga('send', 'pageview');
    }
</script>
<!-- Google analytics script-->
<script type="text/javascript">
    if(document.location.hostname == 'pratikborsadiya.in') {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-72504830-1', 'auto');
        ga('send', 'pageview');
    }
</script>
<script type="text/javascript" src="{{asset('docs/js/plugins/bootstrap-datepicker.min.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('docs/js/plugins/select2.min.js')}}"></script>--}}
<script type="text/javascript" src="{{asset('docs/js/plugins/bootstrap-datepicker.min.js')}}"></script>
{{--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>--}}
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdfHtml5',
                    messageTop: document.getElementById('dataname').innerHTML,
                    customize: function ( doc ) {
                        doc.content.splice( 0, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'center',
                            image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAgAElEQVR4nMydd5gUVb73T3X3DKC7KhlmyIo5rWICQV0FUYkCwwRyjrrmHDGBWTFhXsWECSUpCAIDShDBiIgEBUmTurty+rx/nKrqnqC7d/Xed/t5zjMzPd1Vdc73/HI4wjAM/vNhYeiuHIaBYWgYZlr+jP7vYBgWummgm1owDHTDkkPX0Y2U/J6Zjr6naza6rqPpSSw7JX9aBpbloGkGpqmj6yqW5aCqOqapY9kaabUSy87c3zJ91LQZ/F9F06vQjRS6rmOaNppuoxtWjWe3MDTk0J1q/zM1Vw5DwzSy5/rnDPHHAXGiRY9AyQYkeD8DRgiIg2448nMhGFmAhNe0LANNr8J2dAxDQ1V1DMPCtu3os47jSfC0NLZtBiBqEmzNxjTcCFzbCcCzLNJpDcO05YiePdwQDroWziszJ1NzMXUHUzcCQP48MP4EQLIXvebIAida7GzKcYNFrwmIVoPyLFQ1ha6raFoax/HkzlYlaOm0hqYZaKqFZTmk02lcz0TTKzDMFLpuYlsEVGWjaUZwTR3X9dF1s9aoe3NV3yimbmHqfy4YfxIgNdhTNrVEC52qsdhOFiCZyZumKalJN9E1ueiqKnezpAANTdOorEySrNKqLWIqqaOpFppmUFVVgaqVo2rl6LqOmpbXsy0fXbNxXZ90Oh1dV9f1zEYKn6nmJqoxNwmI9QfW7X8FkOzdX3Ohf2PnZ086+2/dDdiE3MWalkbT0qTTSTQtHfB8CZjnecFwME0dAM8F1wF8ALBsFd1IYhhGIGNsUqkUyWSSdFqyNE1PRgtumnowbEzTxjA0KdtqAaJVn8t/HyApObIfOgSkLiEfDS2iBk210VQXTXXRdSmAbUfHcXXAATwA/GCxARzHI/vluRIMxwLfC4DJGrYlv2zbNpqWxrJVUunyCHgp6NUaz1dbOYkoPprPfy0gtQV59VEDDDOFbqTQdBNNlwLetBxs18DFBEzAxnXdzKJ7XrT4WhqWLlnLxo2b8QHXhW+/3cnKFV9hGVC2DyrKwDIlkL4PtpPByHYNTEfFMNPBcyQDcIxAjhiSImtuMjOFYVZRXbb8VwGSzgIkG5S6wMh8TzckILbj4bjgeTU2te/ieQ4ArutH1OF7kmB+2lJJfvMTKSmegOPJBR8z7hpa55/Mnl9hQO8buOSiK7i07yTum/48239Ks/qzrahpsLPuBZL1hVqbZJdaRrhXkx2aBMOsqkH5/zWAGFlA1KGRGEYgNFVs28Qw06hqCsuSqqrvZwHgge/C99/up6hwAnPnLpYLlsWm8MGxJSD7druc3qkXY8beiOOD68GI0ddy0kkXUl4Of6l3DiOHPcGsZxbz2uurePqJT8lr2Z2Oh1/MuPHTWLHyW1zk98KHsEwP1/UxTRNVlcpApFWZOqYl5ZKqVWFZBqqq/vcBYgY6fKitSI1FAiJ/16io3I+qVaLpVXieh2V6EOxqL2AnQ4qu5sRje6Ml4Yh253PVFdOlXAgWzPd9/ABB34W9u23OOr0/Y0bfieOCacHQ4ddzSa8J2DY0PvRi6iW60rPnJFQNrrnyderndMOx4JRTCnjymbclpfiS3REOX1Kr60rbJZmUCoumpSUguorjyLlZ1n+dDKlJ0lakpeiGhWboGJaKYadx0fGxA3YkZcCsWfN48KF/smu3zUsvrCShnMj7b2+n8+kTyWvydw7skYvvegA2oONjAZCq8ulx/ghOPH4gD9y/kDlvfcXIkdMYP+EelizZRk78NCaMe4Kbb32CzVvSnHziCOKiM21b92HLjzZOoC5YLixctJ6pEx/gvTkbsC15P9e3ouc1TDeyd2zbJp1Oo+smluX8ocX/XwFE12wM3cGynOABLXTdRNUMNEPHtC18PBwvA4btQKdTB9Co4akccsjR3HDD45Tth5zY32iQOJvu515PfeU0Plm0A98FxyUAQg+EPaSTPldcfi/HHdOHIzv2ZvjQO7nn3tk89vgbvPjyIg796yls3wGOB8tLv+Xg+mdwxWWzeWrmSpav2IbtSYL4ZOlGchKHc1THXjRv2pknn3wXz5OUGw7TkuxV103UtInnEnCG/1IKMQ03Yl2qqkqhaFhYthtN3AXWrv+RPXvkAgvRgVmzlrJzp8OWLSmSVdChXU8uvvBa9u+BQxr8jdEjbpFaUiB8feyMiutJKqmqhLQqtamyCgdN86hMWaxf9wOaIRd02fIvOK/rKP560Fl0P3ciixZ9g+9LsI47ph/HHD0Qx4XiIVfQo+dQXBf69pnKihXbpGwDXN/Ddh001cbQfekp+O8DxIh2irSi04HDz4qEtevD/gNQXHwtjRr+jaOP7oXjQDx+DP37X4Vjww03PM64cXfywP1vkdfyLEpX/sznq7dRuvLrjPbl+1Lt9bOEe6AUZKuzlu3i4UvZBPh4+MD+vbBo3rc8+fgHfLJ4Iz5gGiDE0Tz1xGc4Lqxdv5u9++COO2aTUE6kbevu3Hj9IySr/GrKh6FLFmYYWpbM/C8BxDCkmqhqUhW0PR0PG88Hx4GvvtpPvZxjOafrFJ54fBX1c88gnYKbbn2RWM4R9O17FbHYETz11HuYNmzddgDNzFaBPRzPjIzD0ACU6rANONiOLhUEN/h/qJT5PuBh2y5+QFVeYM27rovnQY8eE6jf4DT+duoQBhfdyZbNcHCDLnRo24fSFftp1vQ05sxZhedUv77j6qTS5ei6+qeC8m8AUlOtral7B9ShJ7E9HR8Pz4dUymbPXpvycqifeyIdO5Rw4fm3kxM7g3vufg/HhQcffoOjO17Agg83SEEK2OGud51gdzvVAMEPXCR4gBPIFicAJ6AaL7NwHm70fkaF9vB8ScWuB9PueoWLLprMpo02fXvdTU6sC13OHMtxx/RDTcn7JRLtePP1JbjR5Ww838pySNZcryy2Xocd9h8AIi9sWmo0QmMu9PsYhoFlehiGheOZeLgRm3p85pvk5rZh46Zfeeut9SREZ87tfCODB9yNIo7gttuekUZesHN9P9I6q9kffvQRN1J7CYxD3/UitRgc8CUlhMajl3XN8DvB/g40KEeCH1jzP+8w6XL6FOrHu/LVVyZLlmzjl5+hffvuHPSXjny5aUfEQsMH9V2k20czsCwDQ09hGml0VcPQTOmqj1z2NeMutYERv29Va3VeQIIhPa+aauG5mYX8cH4p06Y9SyoFI0bcSMPGx2NZMLzkIdq37scX66p44YUF7NlnBs5ATyr+ASg1XFBZGztQVINFCBfG88FxfXAt8DPCxLZDkD18AuAidEJAAivTl24Zx4LdO2HKpMeoV+9wNm4q57Zb3yCRewKvvvYJpi03265fNH7eruI7GRaoqiqqVoVtaRh6CsswsU0nAMKpDUi1+E82IHV6XqvbFqE7XGpUHobuRAGhyDdky7W9797XSMQ78ORTb+O6cN454+jZ/R/s3AFDh17Dj1v2Zy18sFjVdnDApgIIMv+0cR09ohzTdzEAjVAR9sCz8QLW5YY7GVuOkLL8kNXJe7uuLVENXq4rcVtRupGbbplJTDmBa65+QbJUD/bthUsuuoJjO/aO8Jes2iStl2d5iKWSY+pGMLLd9drvAFKTQrIB0Z2Mvq27GLqPriJVP8PAtk35oC689cYnPPHYXFwbigpvJLdeK7ZuVVldWoYQR+J6YNiZHe/6Fh5WsIPJ7OAAEAsPSUAO+A6up4Fvgq6DbkIqDVVVkE6CmpIqk2NJ5uY7wT0c/NBRGQES3iPcDHJIT4CLh43jmfjAgXKXosE3k4gdwzPPfIhlQf8+txATp9K00Xkoog2eR/B5Gw8rcO+nA9+YFoR6tQiUatynTkCiAFKKaiHUMBBjmtL6Njx0zUNL++iaE3hhpQB3fRgxbBpCHM3zz67AdaCg4B8I0ZKY0p6NG8skWwtVVseRC4CHixPiELEUP/rVC9iQhYMJdhqSSeYOKmZ2i3asbNOR1e2P4vm8tthLl4JtYltaALJkSw46bjUKCa8vPyPBcCH63Y/sDi9Qr6dNe54J4+9l0vjHyFW6cHibQlYsK+Pcc4ZTVHwZrg+mk+GIVUkNxyYQ9r8VgvhNQFI1AMkWNFoQI5ABGV2z0TUnii34wIZNm3nggVexdTjz1MkIcQQ33/QsO3d6PP/8fD5fsw3HlSpwFmeIvu+GelCWgM/ITMmG8G1MLHBVOFDG570GsvGwfJKH5pFulMcnf2kECz6S1iFOALGN62k46Dh1ABJSR+jSd107ozQEMiWkZseGvbshJ3YCXU6/jH+++B1Dh9xFeQXs3gXTpr3Ijp1pVIPAr+aQTgVs3jCqe8Rrxotqs6y6pX34vq5nYga6kQx0fqnx2A68OedjlFh7+vW6l4Nze3DHrXNIxA/nvvtewrQzrpJwdqbuZNiG70ZWfHWWIvdr9D6eXFRbg7Jy1g8oZGPTNiSbtMZs3paVjfJgyQowPXzbkhAHlBVqU9UAr64tyFuH1Bn+dDL6RmgQvvbapxzRvhdTJj1DKgVfbExzzFFFJGKdGDvublQj9Ep46IZLWrWyqKRKghJGVX/Dys9Se+vSlTNalm5UYphVeL4Uoa4NySrYtdvijjtfpl6iKznK2bgWPPXUXBZ/sj5yb/sEHtQAGM8JZ+5VB8T3CH0VNYHyQeqnZeWsG1jIF43yMfIOx85vx5JDG8PHy8HKogAvjDQG6kE1tZdqf9uOvIFtZal5WZuDcB4evP32asaNv4enn1lEoyZdiYuzeeC+lRx3XH/e+2BlBIjjgm64gZ2i1gakWj5BFiCSJRmYRuZDoeUpY9jySymtQgph38H3Yf2aXeTGO3DOuQOxXZj96gZyEidzzNE9MyAEvqcsHlRrd4af9UNtyAt0Sz/LfnADdmfbUFbJuoGFbGych9G8LVbrtnx8WGNYthoscG0v0tBcgiij50YLLFWJ0EGVATBCqdYzelLmkfnKug0/0aLF2RQMuI9HH1lPo4YXoygdeWrWnGgjOa6PEyRTSBuuEstWI9st1FprgpLFsuqwPwwDx4aqqhSmrUWGlGXCX+qfwPS73uGNN1bwzXflvP/+Vzz88FzmzVuP60swpIZTPfZd00rLYGPj+7pU2byspIVsNqMZUF7J54MKWdeoBVpeO1J5+XzUuDF8tAwMaW/YPtXV5pDKfB8DFwMZiMqWVxlrM3wjvEKkP0ebzPHhsUfnkoidysKF5eTmnsmCBZtxPNizz6Sswo78a7ZtkkpX4LgGqlYVpDNppJI6tuXXAUgtfbh6HlUypZJW9UhNdFzYu8ciJo7k6UdLcW24bMqjdDziAlxXciLXDYWiVX1iOJIKMKPlilgTDp5rVHMgRriFf5sWVFayfPBgVjRpSqpDB1LtWzO/4aHw+RqwHHn5LOXAtT3JHQMtz8bFwosihaF7pjY/yxii+IGR6XtYjh6x4kcfm0sscQI33fIa+/fDddc9gyKOYNzYu6T88aXPLJmsDHLCNCxbxXa0KHOyFssKre6Mg0y6SwxTem5VXYv8So7nytiEB0MKb6B+7ERGDX+Ac7uNpbDwagyTjHpLyIacGiNgTdkGoe1BeRkky6VtUVYFB5JQqUN5So4qFcqSsPNnfhw6jHXN89ib14qyvHyWNW4MCz+CA+VQloJyHcpVKEtDVRoqw2ukIZWEynKwPRxLqrt+lgmaeaga7CsiNg/H9fE8sF14863Pefix+Zx6SjEx5QRuuu5N2rXuyaIF66N18Dwv0rpUrRLTSsoMS60OoW4abqTSZtseoYal6mlpL/hZfNSV87rphkc59OCT6dVzMhu/3B7ZGW4wPz90sUY7LqNK+iFDNkBdvJyV/fuzs38/ful/KbsHFPBz3yK29RrMT30GsvPSQnb2G8wvfQaSGlDIlmP+xs/N2pNqdTSpVkfyXeN89px/Mbv6F7FrQDFbew1iR98h7OhXzI6+BezoO5Cd/Qawo/8ANvfpwxejR8P3P8iASOj4qou1ZoFh2yae50QZLGE0MaVBbr0j6d3rejqfMYUZdy/j1JOHc1GPkZGT0/dATVuYhoumV6HpFViWUaeXuIbrpLa25SMDM54Pe/YkqSh3pIwMfE8H9klDOVThwwwRr8b8as3TDzBSwXz9fRY0b8X2FvlsbdKcH5u0ZGuTNmxr1p4tTVuxpVkeW5u2YmuTNuxo2Y5fW3SgquXRpPOPparV0exu1o4dLTrwY7O2bGnahs2NW7OlWQe2NOvAtpYd+LF5Plua5fFD05Z8fVgzlh97EqzdAJZdC4ffAiSbalzXB2w8bEwHyith9aokHdoOJDd2GjmxE+jbe2yEtesQpbM6riGT+LIS/+oW6jUt9cCACZMQdBVaNjuVK/7xCAcOSEXICZyK4YNbllPtwaV6W31ifsAiXDKA8PpCSpseQVXekRxo1YH9rTtQlnc45XntSbZqR1nzlpTntaO8VQf2tmzP/vzDqcg7kvK8w9mffzh7WrShstXhVOV1oKxZG5L5HSlv2ZGyFkewr0Ub9uW3paztEVS0PYrdjdux7qhT4PONUk3+TcIIrX0vUM49LMuqLtcCIldN6N/3Wg6ufxqrSn/lo4824TpEMjX8bCqVEephjnFttdeQVrppZTLypErmYphetJtffWk5J59QyN13zuX88yby5Zf7mDz1xoicw5fjBII8W5evBogdkTueBKR89lzeatGR1Y3asLxxaz5t2poVjVuwsnFTVjVuzIrDDmF1sxaUNmzK2oYt2dKiPftbH0Flq8PZ37It+9sfxbqGzVjVsAlrGzdn1SGN+axRPqWHtWRlk5Z82rg5Sxs2Z3njViw7NI95x50Gq76QoixLPtQGRBqVXjUtQRqNmfnI/z476yNefL6UnTtgwxdlfPDBet56YykbvtguXWy+dBnJvC/jNzNWhG6kZLzDSGOZOoYmU/iTlXaGDfmQiB1Ly2aX0KplH155+WuKBt1L1y4jfoPkf4sPhP/L0jdtj8p1X7L5kZlsve1edt35MLvveJA906axa9ot7LjjFn69dxo7pt1K2Z13wq3T+a7TefzUoj3pgCrWNmzOgYmTKb/7TvbdcSv77ridX2+9m92338euO+5h993T+WXavey88x623j6N7U/Ngh07JS/xf+dR65ItdXwmzCm79ZYXyWtxIfUSXTnpuAnERCdO61RAOknEunTVkrGSwPtR00siogCUoWHoqhyGJZ/Vl2kylgPzPvyJ4sEPoIiTOf7YIurFT+fLL9R//bz/YjK4lnQWqbqk/SoHKkxIJiFVDulKUCshXQaVZfDTbrYNGs5XjfOpbNaGqpbtWdqwOZSWQtl+qKyAigqplVXpkNQhpUNllfQQp9PSQ+zY+Lb1LwD5d6Yg+bZrw2efbeetN76nWdP+XD71XRLxs7n6qiewQqXSl9SlpdXADVU7+1EYpkw2tgwT13awTJWqqgo8VxrGrgdpDQYNuomvNjnYNnTuUkzjRqfIcOYfmlCWbeI68oamE+wCAheqDa4Bng5GGvaXsWFQMWsaNSfd5ggq8tuzuGlL+HQ5zo/bePauh0j/vB8MQ0pSx5PDtsDSpfvFNGu5Rv7j5/cdfC9w+yP9dvmtexDPOZHrb3oOx4WfduyRiR++rL7S0maQfV87R1iENkhYgKIHmeChVvrVV2UI0YH8Vl2YN/8bJk66j+82J7ECx+EfBcT3rGBknI7hXHFDd4UnQ1Z6EirKWVNQxOeNW5BucwRauyN4/6C/wKrPQYdzz+jFY4/MDrJOvGrZJ1K2OeD6UXDpj7888KW95QBvvr2MWO7hPDpzPpYHTz75LkI5LCOcXMkUUqmqoPSuBiCaamGZHqbhkkqppNNpwoia7cEtN79A/fon4niSdZ3eqYhhQ28O8prs333Uf/nyIduhSChIfbe688/3cPDA1aG8nLWDSlhzWAvSLdqg57ViSaNGsGwFpKF44LUoSke++HJb5CdzvDAMluXX+nfEw7/zClmRn9G43pxTys5f4JFHP2Fg/9tRxDG8OOtjucFcgvpJs87cYKFrTpTspusmmqYBXlAWAHPeWoMQHRgz7ibefnclXbuM4MorHon8On94Vj4yWcEnK1AUBE/s0MsU2NKOCfvLWFc4lC+atkbLa4eRn88nDQ+F1Z/jVfoMLb6bRO7pdO5SnMHa9yMq8Vw7uGegkv7RV7DzQ9vM9uDFf86nXr3jyEmczMB+d3LKScPIa3aWnFpAlmrajBJEqgFiW37kIg5zjAge3valhrX0028RseYo8TzatD6brVt1/gx5GNqGGVHk4XqBn8sPhpdJUsOwJCDFQ/isWSsOtGxFulUeiw77K3y6FGwoLLoXJd6NeomzmP3y6mC/SE+DE6ncGbf/H2ZboWfCz8Tj13zxAw8+/AZfbqzAsUHTYHXplqiQyPMglQyy62sComsyOdq0VNJqJaYpbQrDtFm9ZiOvvr6In7bZWBZYlpSHbrTz/vBccAAz3MHZrhbXolo6S2CzUJZkbVEJq5u1pKpNe9RWrVnSpDGsLMVPQ0nRDGLxHsTFheQ360PlgWARcAM6k8ar4bh/CiARlYWyyg/0CAe2bXd4+eVPOPnki/nLQW25/fYH5Ed9XyqWdQEiK1JV6VsxUjiOhe/7uB7MevYdGjY8g3qJszikwSWc0Wky//znkkxdxZ/w8gNUXdurHtULuVfAXp57+kOWzf4U9iXZVFLChuatSLZoh9XycJYe2gyWfw4pGDb4fuJKL3JFAfWVPpzXZSKOGW4eO6A6uYp/xhzC5/exI3+f48F11z1OXDmaROxvNGncldtvf5otP/0abABHuk00s0bsKQBEFqikSKuVuEHGhe9Ldb50uU6vC2eQEBfQoW0BM594CyfivX/GjMBzMgGkaITiyQNDh26dh7LwhaWwL82aAQNY27AZWov2EpBDWsKnayEFwwvuJy56kxDDSCiFHFT/Qp5/9tMg49CRcigsjfsT1CwZkw9DDTau7+G6sG7NTm68/kU2bYDS5SYbN6QxzNDd5GFbBkZWGmoEiKYZQXGKzEz0PInyzu0q3c4ejSI6k9+skOdmrZfGbUSaf47SiOdLoY7UhlzAzMo+tC147oWFJHKPZ9nb62G/yoYhQ9jQNA8trx16fns+btgClgeADJ5BLNYLER+CiA9DiD4ce8IofthaGai+0v0fqvV/fBYZuee4Bj5W5BH/7rsU3br8gxylG4lYJ26+8UU0NeDArommVhJFbENALEuyLFVVSaeTkr+58P0PeykqvJ4Tjx+GECdz+unFXDpwIr/+mgpqAv8svTEMZnkyOcB1I2XEc6XcatK8M0I5imXvrYdync8KClnbuDmVLfNJt27LwsZNIpYlAbkYES9GJIYjcochci7iquufxLDlAuJbEvhAdv3Bp88aDgRRUk2DgQMvo3XeRdxx60LuvWsxh3foxYpPvwso08MyZTOE6naIlsZ1XTQ1qPtD2hcu0lCuTMG331ew4csDLFn6LZUVbsCyMjGOP/LKcCkHGxM31O5cSd59et9ATs4ZiNjRlH6wHvZpfFZQzLoW+VS2bkVV23wWNGkIK1dBCkYU3EdC6UksVoiIDUfERiNEP3IanEUyHTxukO2SpYX+4ef3PEeWcfsWvi/jRd26DuG1V9dh6PD9d9CgwUm8PvsTGbk0PQxNNkOoDoielOxKs0kl9UAb8bA8qEq7bN1exc+7PC66eCJFxddm/HE+uI71hycTGm+WrZFJ8ZTv//ADCNGVePx86tXvxCdvlsI+kzUFQ1nTrCXl+XlUtsurBsioQTPIFReTUIpRlNEIZRxCDCGR0xMhjsINPa/INLo/A5Bwx4c2VJhEfscdz6OIdsRiRyLEEZxz7nB2/+pELqc6AbFslVSqCtNwZYQLSQGrP/uRNq3OI7/lJSjKGcSVk3nu+SVRudef4QuKqMMPDB4P8FwZkVShXoMeCHEpObmFKOI0St/6AvZ6bBo8ilWHNY0oZGHjRrBiNSRh1KAHyRV9SYjhKGICQkwilpiAEINIxM7nwXvn4Fl//Nmz52AFBqxlZVUOeTIFYM6cz7jhxmd4ctZCdu/JyGDw0NTKqEtF9XiIoQU++swOfenlBcRjx9Lzwtvp0L6E4UMfwHHlBe2sOBTBxf/Tl+sH33W8TGzLhYmTXiIWH4CIjUSIYnKUsyl960vY67Gu/xC+aNGaipb5pFu3ZlGjAJAUjIgAGYkiJiGUyQgxHkUZTU5iIA2Uzny1rkzexw931n/+/KGvTEYRyVjtPmz6egczn5jLo49+yKznl3L3Pa/y3vsrpJvFM7DtqtqAWKaOlpaS3rZtSSGeS3mVxbtzSykoupFE7vE0aX4mr8xemkE4VFd/Q5b8a7+j/LyMUYcmovx7/oJvOfSQgcSUYcTEWBSlCCXWmWVvr4UDNmsKhrK2aSuSeW1RW7Xm40aNYOVqUGHY4BmB2jucmBgnhzIeIcaSiI9AERdz4d+vp7xK5mh5USJGBqBQ0wzdLeEmrWs+PmA5GReSbbuRkTj79Xnkt+rCaZ2G0bZ1HxLxkxg69IYAEBPLqaot1E3dwDatKGM7VActR25ay5VZ60VDbuG9uWurAZLNN//ngNQ9ub3lcNElN5GIDyAuRhET41CUIkS8M5+8sxbKdNYUFLG+aR5qy7YY+a1Z3LARlK4CFYYU3Udc6UVCDCMmxhEX4xFiNEKZgIiNI1cZSkLpwdPPfRQkOso5hDm+juNEz2I73r8ERFYXy/lnV3E5juQkjgNz3vwRRZxJSeGD7NsruQx4MuGhlpaVllFCwzCiYk3ThgMHoHDwHUyd+jKqCtNnLGDO21mAeGQJk98i+X/DrepnvCSuB08/W0oipzuKGEyOGEdMjCUuBqPEukgKKUuzvmAgGxs3x2jRGiuvLZ8c1kQKdRWGFd5HXLkYJTZcgiDGIWIT5e/xQMArfTj4kC6s3/BLsPB6ZkfA7yZpVEdFguljVwtjh97fb74po23rPiiiM5f2fpB7pi3inXfWyGg+tF4AACAASURBVGz5oONQLaHu2mAZskOO67qRn+r12WvJiZ9GTvzvJOLnEE+cwmOPfyBlb+DO+G0S8OoYvzNBX97z88/LEKIzQvQjIYaTI8ZHLEvEzmHZnA1QluaLgkvZ1LgpZvMsQFasgjQMK7yHWCwDSCI+mXhskgRGjCE3dwJCFJCT050xo+8nFGG+l6nslY7CmpPLCjtngQFO5IPzfV9+z5f207z5n/H3c8dydpfxnN5pLMcc05e77n4mAtx13dqA6JqD43ikUlUyqx25Wx9/bB4DL53GtdfMpeFhF/PKPzewY6cX5a1GDr9atJzNxmpJ/zrw8HB8aXOceOII4qI39ROjSIgx5IpJxMQ4hFKCiJ3Hkrc3QZnO2oICNjSRFGLktWVxw2wKuYdYrGcgfyaQIyYQE2OpF5tIg9ypCDEKIYaTIwYRF12Z/XIoZKUD5LdZ7W9lN9aYjx/UvwSGbnZaq4MUAaESEIY7qgGipmVrPFWrQtOTkl/68N5762jdqgennTKe+vXPplvXsdx2x9PVih79TKb0fwQGEBTkw+zXNyLEucTjJShiDAkxgVgwFKUEEft7AIjJ2oLBfNGkJWrLtqit2rKoUZNIhgwffA9x5eIAEClDcsUkEmIcQoxESUxGxMaTK8ZQXwzgrw3OliUoweYIc3Kzy6szgGQDEPiwAhdSJHsiZcvD9S0cLJxAdQiZSrhcEpA6MxcNLFvDcTP9SGa/tggl1o547HgaN+3G2WcP48P5a2Xig5Pl+fOzd1QdgNQCrAZAgVXevEV/4rmDEfGRCDGW3MTliACQuCghJs7j07c2wn6b9YOK+KJJHsm8tlS1ac2CJoFQT8GogvvIFRIQERuHiE9EiLEIZSwiPh6RmIhITEGIMeSI4eSKi5k84QlsjyBeIlly1J2uLuXFz55LUIGVLU4Dig/Nq1/3GkH/FGmzuEGE1FCd2oDINqsqqXQFtqNnvL1e4L6wZdK5ZcE11z1ERYUZWNJhIOm3AMniuTUmEKm8SFZxSqeh5Ob0R4hhgfCdghDjiSkTUcQEFKWImNKVT9/aAPtN1g8qYm3TPCpataWybWvmNQ0AScOIghnkil4klCESBGWsFObKGAlQbDwiNom4cjkJMY5cUUxMdGHegnVYnp9xOAb2hPzj96g+qOjN4hZu8LGXX3qfWLwF8XhbhGjDZZfdF6VW4YGle7UB0TQtaAYpI4ahYC9d8T1/P280/3zpSzoePojdu+Avfz2enTsqAnUwo7/XpoAaDweBfAprw6Uaabvw5KxlxOv1QBFDiSuXoYgpxMQk4mK8VFmVcYhEISJ+Bp++vw72J1lXVEhpsxaUtz0crV0HPjjkr7CyFHQYMmg6OUpvEqKYemIcidAWEWNRlLESFGUCipgSsMUR1E/059jjB1BeFTYu8LKEeradlTW36GXLIqYaFKKm4KILh1BScg0L5n/PDde9SPNmZzJ/XqnciDZo6bpyew0L6fFNkUpVAZIyvv36AOd2G8sDM5aQUM5k9ivfcNhhnVizdmtk+ofNYDKvrN2TsayCyWVSM50g/3f3Huh0xmXEcwcFxttUEmIKcTGRXGUCihiDiI1B5BYhcs5i2XtroCzJ6oJBrMxrz542R1PZ5ggWt2gNpashDUNLHiQ33o+DxAgSYiQ5YiwJMY5EBIgcMTGBhJhArjKeHFFMPH4el10xEwfQHUN2AvJqb7DaUt/GxwrCCJmPJ6ugZ48SSkquwrbgkYcW0PGIHnz4wYooDqOrddUYZjUsDhvHhDmpQ0vu5NC/nE9u/Gxy4qfRsOFpvPveSpzaygU12VEtCgkMrLC7guvBFVfNQojuJOIjEGIciphMPWVqQB1jiMXGIOKjEYlBiNhZfDpnLeyt4qNL+jDv0HZs+GsHNh7UkrlN2sInqyAJw0oeQYie5IhhNIhNlCAoYyWlxcbK68VGExOjSQRDiGLqxYtpdFhfPlr8TbUY/28DEcy6RhjCCyjE92DF8q8ZN/4GHA+EaM2AAVMpLzfwAcMwSCYr6wZE8rFM+W54wQfuf4fzz7uSyRNeZOH8X/npJymsXIj09+qAZCGVNYkwR8pFZmU4PmzebJOT04WceAExMVq6NmJTSSiTJGWIEcTjYxHKKESikFisG8ve3QhVJltnPMBPo6/kwJDLUEdNZfWAYvh+i0wDKphOTqIfcTE0sNDHBWwqGLHRiPhIFGUkMTGSuDKKRExSUVwM4KJLrsawgyBZHdReGxD503XtwFL38Fz4etMebrrxGb788gCOCytX7sZyMuXemqZh225tQNKqiWW7OI4TCHYN33ejrEXDgGQlHNgPe/dCebnk/V4tw7BuQLLnYjoSDMuFnJyzEKI79RJDEWIESs4ERGKCtBNio4nFxqCIUShiDDmxEhTlHOa+s1HeIl0ByaAYJ1kOejnYaSorXIaXPEhcXET92CipXcUmIGITUCIVeqx0y8dGS8dlbCTx+HgUMRlFDCMmzmHmzAVZXtkaG6zuP6km+H2Ycc8cYuJUEqIzubGu9O01jbVrKzBscH0LTbVIp+pIctB0E9vxSCaDuIiRJAxDLlv+Bc2bnUYifhx5LbvToP7J5OS0Z9mnX0lqruaCr5tlhcZWGBAyHLj9zg8Q4u/UyykiJoYixBipkipjpcwQI0kkJiDEaGJiDDFRhKKcxwfzNwcdReVOlCmcskTOwSZtQFHB3dRX+pAQwxFiIiI2CaFMIiYmBUJcAiLlyWiEGC7vK6aQG59EQumHECeiab/ni8v4uMI2tiF1OI5FVYVDfrNujB81i04nT6XH329DiNN48MEPZY2Sqwet0muXRwvTNmTZmi2bcqXVSkD2z1UNlxUrtvDll2kmTXiK+vVOpeFhp7Bm7fbqVvrv2Bk+HrqpRYB8trac+vUv4KDcEmJiJLlKoE2JiQgxESU+HiHGBCrvROolphIXJQjRmXnzvgsKhfxMP18/LBJyUVUoKb6HePwSqULHpiKUKQgxhYSYQkJMQhHjUJSxkSdYiUtNThFSiagfH0n9RB/OP2/cb6TKBu6SCBRHZnBmrUWqyufUk3vz8ksr6HbOMAwDYsoxfL9Zx3LBxsK0VFQ1VZtl6absjaipFrbtkkpVBcF6KYA3/3CAIUNvIjfnBC65+Go2fycjXk5W5LC6rVH9px8kLoX5Sn363YEQ3clRRpAjJpITeGNzYlOJi8kIMYZEfCIxMYWYmIIQY6mXKEHEuvDu3K+iYn7quHey0qW4cDrxeG/JjhJTIgpRxCTJssQElEgdDgW+tFHiirTmG+QUE4udzksvf1It9yxTxJPtnnciw9AKio5feP49Xn9tFa4Du371uPX2J0jktsUOZIhmqFFlbh3Z7x6abkd9E8OTCMKStZdefAchmjB37jrZsdOBRx55i+8275EpVH7gLgi8v64rxbePg+cH6Y0Ba5356BJiSndy48UkxEgSIrNTc8RocsW4wF0yiRwxFUWMly74+CBE4mze+fDrWrs286uDmoQRBY+RKwYSEyMDYT4akTseIUYEgn0qiphETnwcQhkWKA1jEWIkQowiLsaQEEOpl3spJ5w4hr27iWShTPq2cYPNmonlSM4Q1hO2bduFnNgJXHD+VPbukxvxl91qpL2pqi5bqtdxuoLQDVe2+TZtTFOevSF700qVr6zMxPdg588GV1xxP4l4B4TI55bbZkZpoAC+5Um5EjSVsXwDP6ykcuGLNRU0bXQJ9XL6IUR/cuPDAk1oOLk5o4iLQSREQaACD0cRJdSPjyQuhpCIDULk1AYk86uXBcjjEhBlmHTD5I5G5IyQ2SdiCEJMJCYmIUQJObkjZGaKGIqIjyIWG0W92Ajiopi4KCInpzeX9r8Jwwydj64EI7irbJbmRPe3HenRcG14+81t5CZOJx4/iliiZcSywwoq2dyszk4OYZ/zTOdmTa/CtNIRq/nyi58RohUHH3Qsgwuu4Z231zFp8nTKKrJymyL7yZM80pZyw3LB8mHYqEcR4hwaNSqkZf5YRKw3LfMncPBBxRz212LathqOEOfSpv04GjUt5sgjx9Cm9XDiooBErAAl0YV3P9wUya4Mywi0O99DS8KQ4gdkXlaiMMjLKkEohSTiA8hRBhEXo6TLJDEeIYpIiEEI0V9mqIgiYqI39ZW+MgYjLkXEu/LmuxuCSdrYtp2het8D7GCtZCXV6JHTOeWE8XQ/71Yu6XkbZ545hJNP7RF0Z/Uy9p6p49h1Fn3KJK2wyYxpqZJKjGTQZEbyxssuv4u1a3dJddiBQw85nenT50gjMkvkWU7ooJSpRJYPz760mlj9Hoj4xUy94l2m3f0pQjmPO+75nIKC57jmmg957Y0fGDH6EZ59+WtGjJnJ1m2wbRvERX8SsUEoibN474NN1dphZPe7wvdQkzCk+H5EvCciUYCIDyE3XsxfGxQwYdRLXD7xTRJiADFRgiKGoogB3HT9J5x04pWI3IEcc/wN3HTdB9x54wJyRD9y48WI+CV0PK4EUycjOLMUGun7C1iYB2NGTSdXOYO4OIuxo2fywxYLx5cuGQ9XHpVhyTocrS6hngFED9pnZ87wCPO0QhvCcqVhZ9lwxeUvc1C9TuzZnQ66+ji4yKaUnmsAhuxgbUBuvZ5yF4reTH9gGS+9ugmR6Mzzr2xnxKgXefLpdbw/73u27YbCYXdy5z1z+GTpT1w64DZisV4o8T6InDN498OvsxyugesmXCAgmYIhRQ8RF72JxYagxIYQV3pxxZRXwYKqvZAQZ9Ov19McfdRVPP/CdiwbiobczaHNerN+E7z7xnp8HS6f9DZC9ENpUIxQzmfKuPtkH+dAcJqWE3EHz5PFXzt/SeF6sHTpFq666ilyc49m4KCJUQxEz+rQ51gullEnhYRN86WREjahiVr6WU5EoS6wYeN2zjxrMEIcS17zC6gqk0/lAFZYZ+x7koxcGDvuRYS4lNzcEeTmDODOaR8y88klxOudwcyn1nPpwHu5/8EFzJ6zCtOHwUNu5Iqrn8B1YMrkJ4jFeqLEe/8GIJm0Gx9pK0pA+hJThhNThnFet2ksXbyXt15fwt5dDnHlVD7/HI47firNmvfFcuGSvlNocMiZvPPe5qjC7tROkxDxixC5JYjEpSSU0yn7BZmY6MqP6YHm6Prw0CPPU69BC66+ZgaWKYnpu+911q7/ETN4zLSqo6blQWVa+jdkSNicMWrSqOmR9Nc0A8sOOo26MGHiLSiiLYn44Tz/widoGhzevicfLdocRcZ8l6jfyIIPvicW64mIFSDEIOLx3txz53wWvr+FI1r15eVn1/Hwg/N5+umP+WjJj6xet4+FC3/grmmzeXD627Rq2ZN4rCfx+CUosTOZO/fb6n1MAAdZXeXjkExDUckDiFhfRGwkSmwYBQOexglKDU0fxkx6jiOOnUCjZoOoV78bjg1XXj6DwoE3kKqAk07ozo6dsHy9hajXHZEYjBADief0JT+/bzQ3E0kwti89Fy5QUDQBIY7h4PrnUDjgfvJbdGfnzwaODylVJpFYOpga2IaPWWc3IE2mxdumg2XY6Fpa9gg0dXnckKHLXN7AA3zj9c9TfkB2q543/xu6n38N3c6eTDI4lyPUqhwNTj2hBEW5kHjuIERuP5Sc7hQPup/dP8G6lTo3XvkKrgZ9e1/JG299Teeu43jzjY088fhi9v0Cjg7xWA8S8QuJxU7ng7nVZYgTASJjEum0Q3HJvYh4D5R4IbF4AQnlAmLib/QfeA1Pv7AUUe9MPliUYtiIZ1ixQufj+T/xxWf7eO7JJVx52eN8v1mndM0+Tu4yARHvhlJvACI+ECXRHxHrys3XPyPnF3IFF957fwWLFq/F8eCbr6BF0z40SJzNUzOXRZmZmiaP6jA1F0P1srqU1ooYpjGNdFbnTEOWSAdt6VStHNNSJWdwYd2aCkYNn8FHC7dxyMFn8uzTm8hVzuC+u2aDJ+0SNW0xquRG6ou/kRvrihI/F5HohsjpSkycSbOGF3HQQd3ITZxO62YXkRCn0uCg81Hi3TisUXcOPrgrjQ+7kIMPugBFOYd47GyEOIH331kbgOEFnCVMlvZwfIPKKpW+fS4jJk4hN9GdhNKDHHEuufHO1Ms5HRE/GZF7JiJ+BrHEGSRinchRTiU31omceCcSOaciEscTa3AqInYGitI1uM4FxBPdiSc6c9hBx7Ni8TpCH7rjQs+LJhCPH8Ojj3yMY8Njjy2i4+E9SaellmpYOmm1Uh7nZ+pB+XkKywzbJ/4mIEYtQGwnTVqtkILdhPL90KzJWdx711y6njWZmDidvGY9Wfrxj1Ht5oH9lTzz+Ds8NmMuMx9eyMzHF/PoEx/z0GOLePihRTz26GLum/E+jzy2iIemz+XxRxbz+MxSHnhoKQ89uJAH7v+QB+5fyMMPLuHRxz7lkYcXMmP6G/y0tSLYcU6QlytvaOsaIM8jfPThVxnY7wqGDbmLksK7GVo8nWElMygpvJuiwjspGjqNkuF3UVx8M0VFN1FceBtDSqZRVHgnJUPvYlDJLRQMuZ3hw2YwfMj04Br3UjT4HoYPvZsBfSfx8H1P4+hSoRg16ma+3GBybtcriYlT6N3rWr77Xuezz38K2o/46KYW5F9pASApDKMSq45TekTNZjMhICFQoU2ialWRRlG6YjMJcSwJcQo9ul+B48CWrRqbvtolBW6oAQT83pFmQhQWDitWwwNVfF+CHRm9bpZu4Af5sIFaKZ2VYS9eBywvcDLLCibDkJ/TVfBsWV/iu+BZskTd9WTecHh/15MaZNgk2fbl75Yua3t0M7i/I8vkXRO0tI9jwZixN6Aoh3PwXzpz1BFFPDD9U2LibxQMvCnql6UbFppuYugupgFhDoPsnPFvAWJVY12WFRxzaqSieLvjwDVXPsqr/1yDa0O7wy8g3qA9QjRhxJCr5FqZGYvRx0PXzehvcHA9gzCka7sGkXqDCb4RpSRt27GdXbt2BT1UgpiKGxSEuo5sMKA5+F6YekOmwiswkBxbbiTHCXKv5Lv4GHho+MHhAbZFxncVsKQoazErZcTzoLR0K+UV8NHinZx4UgkFA+/h5Re+5q8HnYVjZ7EqTUXT7ayjXN1Amw2Pbq2p9mZ3AdKd6IuZfuUORqACRzXsWfGSxx+bh4gfyQOPv0faBEUcxTVTZ0addizfJmzfIp2RLvgSCBcn8A052G6oMluAiY2Fg0NlsoqqCtksxHEcrECYSqszQ034cg3NgGCkAzIAP5I5wcsNDIegDWE1e8aTz2i4Qb9fTNnYIMTDg/nzSqlX7wSO6DgAy4WPlmwnrpxMPHY8n3++C9cH6bRNBazKkk2oNa9aX7K6+2XVBET3MXQ/AkRXLRzLl823NGn2E+4aH8aOvYMLekzhux9cLvvHk+TGT+aGy2fi7VFlTw49CUYKVE3Sv5qW59klK6T+Z6Qlf9E0GYg2VbBScqiVUJWUJ7bs/lX+rEpDUgPNllX6aRUqK+UCOw4kVdBMee/KclDLwErKninJSvkcFcE1U1VgJiFZJnuiqA5UBr1QqvbJwFe6AsoPgG3i+/DZ6g2c3qkXUyfPQohT+fsFU9n4tc5HH22ndNUurICLWo6JVuNgygwYGQOxTve7bma/GZyKEAh42zJIJSuxDBNdtVDTjmwh4nh4HixYuJ78/K50bNubmDiWN99ZCxaYpZ/zRnEBS0ePYunwoTB3LpuvvY7d113L3P6X8tG0aVCVJPnsC7x+7gX4L77CN7feBqtWsvjqK5k9fCT8sosPe/XGnTGded3OZv+N18PadTzXsydf3ngNlU89CG+8wit9LpRm+OavWTRsGKUFRbBqFbPO68qrk0fBD5ugdDnvFgxk5djReM/OghdfZu2kyfDVRv7Z6xLWXXEVvPUOxlNP47/0Es+e/3fKHnqIzWMn8F7f/mx64zVw4PmZr3PdFY9z//RFNG/Rh6OOHM5pp49jx06pCrtAUgtYlaFGBxfU7GytG1aNdY+8vU5g0mfF1c101LPc0FPYloZtGWhpHcuAqkojKiF3fbj15pk0iB/Day+V4viBtNy/n+VDS9Bvv52vB1zKvokT2DqwgF/7Xcr2YaNw5s+Hn3+mtO8guHka+6dcztoBg/jlumtYNXQI71zQE37YykstWsAdt+GPGcXCjh3ZP2kyZddezffjh7Br8gi0CcOZ1bYFlP/C14/PYN/lU9h8fnfcSVNZesH58OE7sHsra8ePhKdnsqLn+eyfNJ6qsRNYdkYXrNtuZddlU9gyfBRfDyqEu6fxfOtWMH8+W4aP4ftzL6J88lWw9SfJIi3Y/TOcddZwTAv+ctDZHHVkP/btl4BolhsclmkGC5519HdwtKuum8GBab/jOqlJIb/5t545+EvXzUyQKtCqfJAdF8qSLBs7Hl55ha2jRvJy63x+vLg31pDxvHd8J359/SXYsomFF10Mb78Hm39g6QXn8+zRHflySBFLelwAP2zhlfx8uOEG3mjWHO6bwaa+feHtOfDTZsomj2NZ63zeyGsBe3ay+Oar4OHplBUWkioZxWvHnMSH48fDgZ95/9Ke8PF7sOUbqq66goXtOvJRu47sGzsaXnwO7bobWXPBhXD9lTyb1wS2b4clK/im24XMPeFM2FUmO2cTeE88+NuJF9Gi2fFREwVVk+ezh2e+V1/D2tRQ1/hDRx6FQinSaEJL3XbgQAVzp15GxYuzWPuPCfDO62ybMI7v+hWx+9qb2TfvbUjvZf11V7JqaDErR5Tw+cTRsPpTlo4oZM24Eey44Wrm9+wOzz/LD+PHsXnCeNyZjzJvQG9mnXsmv952Pcx+mZmtW0LlXvYufJ/lo4tY0v0ceOIpPh48RMqR5F6+e+h2vr79H9x18pFsueEqeOElvisYDK+9xNuXXMDyoUPZd8etOA9P4+OBPVl/3dXMHVzEZ/0uJXnfA1QtWwW2j4snm9w68I/LbiZZJbVHQ3exTI9UUjpo5SEGtSngfxUQwzDQDDUTIwiihjgWmDo/L/8Ye8s69q58H3/TUvQl78Kni/n+0YfZOOcV0PbD1q9Z+eg9sONrDiz/ELas58DyufDjOj6ecTN8vw6+LIXVi9n2xgtQ8QurZz2I+uUy+HYVfLGYTc89BGY5qPvZ8tZzlM19BX7dzpa5c8BOgVUOqZ0sfux27G9XwXefw5qlHHhvNlT9zGfPPsL+ZQtg+9fo6xfALxv45L47YesWUgvmsOzBu/jls9XgerhIrdH3iApI1bQZOWUt0wu4x392LtUfBkTXZY27G6aNA65jgGeCUQH2XnB+BX8vpH8GdT+oFaBVQPoAOEkwy0DfF4y94BwAYw9YB+TQ9spFtSpA3y8/55aDtQ+M3eCW4VpVuGYlmPvld7UDYFWhmUk0sxzMA2Dvl/cyDsj7WAfk+14S/LT829stn1Uvh6rgOez94Jr4toFst25FG1BXZfc9XbNRVT1i57K/4v/8rNw/fmyebqClg06bhh5g4uBaKr5RiW+Ug1WOZ+wDpxJfKwMziWemcI0kvlYFZlp6I620fN+sBFcFvRIvVQZWWoKop+TnbRVHr8KzqsCqxNXK8W0DLVkBjgpOCj9dCUFUzrcN0II2gaaOb2k4ZiW+kwJDxdGl0atbVTheJbZ5APRKeV+7HM8qD2I8gTXj2XiGg6u5pJOyJWLmoEnZiEHX9ejA4/9TQCxDkqdm6FSkkmh20JY8zJkKzv+QBp8dkLxF1JnHd6SPwgmTt6Ux57lmYMplPuc6RvT/6GfQxsmJGhsGn3McoiMZPA8sI4jR+HiOTMKwHD0wVGWdikvYidTKXMcP2gviyLI13wHXxddMrLSBZdg4jux7lU7LKrRQdvx/oZBIhXNs0pZGUk/JE6FNXZq1thMVZ1ueG2iOMqEM381KEiAqmpSHq7iEydkuYWftoKmAG6T/e75UsYPWHGbgFQidjjJ1JgDcd/CsTK/FsEFCVEYQXi9oshImvsncWQ/PknLSMkz0VBpHs7DSBoZmkk5Lw9p1Zd1gVVUFnudEfUz+bynEclA1A9U0UC0NzVCjppqWpgdRqzCiKFVG2f8hTH0MfFMQVcKGPiTLtwO3R+YICo/MqdEZddvHDWqVbNzMMRV4RM1mPCsTB3dDp710enlhR2hPAuO7YV5J8F7gr7FNJ7DFbFzDwVLNKAyb6aGv4rqyZtOy/ufr+Sec9FnDORkYlHYQX9ENB90BGWUPz0UIdqIfLnF2GmqQ4xRE6TOAeJndn0lxyYogZrrFZcwiL7hjprgIQq+yLym01v1rv2zbRVcN1FRauo9+7/Cb3z2E8/8QkMjVomvYuoajSUBUzaLSsKhyPDSy0rF9T8qXYEFkhotbbWFkIMoP3I9+4CYMPue7RG0UfCJKiNhfDUBkc00/k7UfeqL98BSs6oC4vjwNwgiPItcyJ0joATdQrd8D4r8EEEkdGp5qZIFiUKkapIImBbg1DnYMFyj7zGw/83bNYsnoj4DrSZd5KOCJPL9eFOiVlOPhZxLEf2s44NoehmWimgZpTR7ZoVoahhWccGqm0a0Ump3mjwLwpwOiB46ybPI1DUkhjiYBMCyTtGGS1lT0VBJTlfzV8exMI7SaC5O1YSMgsgGRKx7F16uxMS/rS37GBe/WdZ9g+K4nG0kb0teU0jVSekgVEgwjOFcljKbKfvk1Qfn/CIhuBsK8xoicaoZG2lRRbZ20a6E5BoauYqppTDWNoelYloVt25H3uBY4IQVkRRfDnFo/63NRWms2mKHnIEspyCY1Dx/LdjEtp5rTL23oqKb1/9q7luWGQRj4/59boyd2vD0sBsd1ZjpJp+5ketDBj8GElQRCKAsJnlnLzv07Nl+rUeJpIB5y4b5oIQ9A2QNSUvFRFVINnkaeWHNk+7+o7ZCeOWvmOzhn7uV42e61jdhDomk855GhlaFIBdkJcrMEh2irAnCDBS0/ovZ5sZ8QSZLdpDbF+kuA9M62hrfcisVYbWXQCg0zFgAAAP9JREFUzMU+MHnh0jgD6UtzDwZ1MvlMphBjxL+1f5uXHgusC4kguaPUOEJb3DAAYVxya1YzLyvm24KcA1E557Fgv/ST/mYG21l2uKJqokpgKYYqxmSdz/BY6KJ7wuk4LvlAfhEQatCwGPcxuaeSCiOC1iHhKB73qcygj5ZU9HoVVYgIt2WKIERJpWECC+2rnE0hSjpKev9mccMUwRjJFCJTE2lWKX3w3fVOkbhKTMyaWDUwqxOAVilAWeBxRhB5ISAbKGd1DvfPxoeHJn7t5FkGbd/W8V0LbyBZdzeulH79wm/ay/cH+jX5gWXvtbLNP1f34x+QN5W3BeTZjN3V8gnuU7DGdIX0pQAAAABJRU5ErkJggg=='
                        } );
                    }
                },'excelHtml5','copyHtml5'
            ]
        } );
    } );
</script>
<script src="{{asset('js/print.js')}}"></script>
</body>
</html>
