@extends('layouts.app')

@section('content')
    <div class="tile">
    <div class="row justify-content-center">

        <div class="col-sm-6 col-md-3">
            <div class="widget-small primary coloured-icon" style="background-color: #E5E5E5"><i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h6>Members</h6>
                    <h4>{{number_format($members,0)}}</h4>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="widget-small info coloured-icon" style="background-color: #E5E5E5"><i class="icon fa fa-building fa-3x"></i>
                <div class="info">
                    <h6>Zones</h6>
                    <h4 >{{number_format($zones,0)}} </h4>
                </div>
            </div>
        </div>
            <div class="col-sm-6 col-md-3">
            <div class="widget-small warning coloured-icon" style="background-color: #E5E5E5"><i class="icon fa fa-handshake-o fa-3x"></i>
                <div class="info">
                    <h6>Last Attendance</h6>
                    <h4>{{number_format($count,0)}}</h4>
                </div>
            </div>
        </div>
            <div class="col-sm-6 col-md-3">
            <div class="widget-small danger coloured-icon" style="background-color: #E5E5E5"><i class="icon fa fa-money fa-3x"></i>
                <div class="info">
                    <h6>Sum of giving(Ksh.)</h6>
                    <h4>{{number_format($sum,2)}}</h4>
                </div>
            </div>
        </div>
    </div>

        <div class="container">
            <div class="row">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" onclick="this.parentNode.parentNode.removeChild(this.parentNode);" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <strong><i class="fa fa-warning"></i> This Year {{$year}} !</strong> <marquee><p style="font-family: Impact; font-size: 12pt">Total Pledges: Ksh. {{number_format($pledges,2)}} &nbsp;&nbsp;&nbsp;&nbsp; Paid Pledges:Ksh {{number_format($pledgesdata,2)}} &nbsp;&nbsp;&nbsp;&nbsp;  Pending Pledges:Ksh {{number_format($pledges-$pledgesdata,2)}}</p></marquee>
                </div>
            </div>
        </div>




    </div>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="tile">
                <h6 class="tile-title">Current Month Summary Per Giving Type</h6>
                <div id="piechart_3d" style="width: 100%; height: 250px; "></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tile">
                <h6 class="tile-title">Attendance in Sunday Service for the last 2 months</h6>
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="tile">
                <h6 class="tile-title">Ministerial Teams</h6>
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="pieChartDemo"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tile">
                <h6 class="tile-title">Sum of giving per service  in this year {{$year}}</h6>
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

