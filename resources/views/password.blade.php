@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="tile">
                                <h6 class="tile-title">Change password</h6>
                <div class="tile-body">
                    @include('includes.message')
                    <form method="post" action="{{route('postpassword')}}">
                        @csrf
                        <div class="form-group">
                            <label class="control-label">Current password</label>
                            <input class="form-control" name="currentpass" type="password" placeholder="Enter full name">
                        </div>
                        <div class="form-group">
                            <label class="control-label">New password</label>
                            <input class="form-control" name="newpass" type="password" placeholder="Enter full name">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Confirm password</label>
                            <input class="form-control" name="repass" type="password" placeholder="Enter full name">
                        </div>

                        <div class="tile-footer">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


@endsection
