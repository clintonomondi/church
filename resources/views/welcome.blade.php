<!DOCTYPE html>
<html lang="en">
<head>
    <title>Life Celebration Church</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Work+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="page/fonts/icomoon/style.css">
    <link rel="stylesheet" href="page/css/bootstrap.min.css">
    <link rel="stylesheet" href="page/css/magnific-popup.css">
    <link rel="stylesheet" href="page/css/jquery-ui.css">
    <link rel="stylesheet" href="page/css/owl.carousel.min.css">
    <link rel="stylesheet" href="page/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="page/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="page/css/animate.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">
    <link rel="stylesheet" href="page/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="page/css/aos.css">
    <link rel="stylesheet" href="page/css/style.css">
</head>
<body>
@include('modals.map')
<div class="site-wrap">
    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>
    <div class="site-navbar-wrap js-site-navbar bg-white">
        <div class="container">
            <div class="site-navbar bg-light">
                <div class="py-1">
                    <div class="row align-items-center">
                        <div class="col-4">
                            <h6 class="mb-0 site-logo"><a href="/">Deliverance Church LCC</a></h6>
                        </div>
                        <div class="col-10">
                            <nav class="site-navigation text-right" role="navigation">
                                <div class="container">

                                    <div class="d-inline-block  ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu h3"></span></a></div>

                                    <ul class="site-menu js-clone-nav d-none">
                                        <li class="active">
                                            <a href="/">Home</a>
                                        </li>
                                        <li><a href="{{route('login')}}">Login</a></li>

                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slide-one-item home-slider owl-carousel">
        <div class="site-blocks-cover overlay" style="background-image: url(sidebar/images/bg.jpeg);" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-7 text-center" data-aos="fade">
                        <span class="caption mb-3">Deliverance Church International</span>
                        <h1 class="mb-4">Life Celebration Center-Nakuru</h1>
                        <p><a href="{{route('login')}}" class="text-white py-3 px-4 btn btn-primary btn-sm">SignIn</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="site-blocks-cover overlay" style="background-image: url(page/images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-7 text-center" data-aos="fade">
                        <span class="caption mb-3">Deliverance Church International</span>
                        <h1 class="mb-4">Life Celebration Center-Nakuru</h1>
                        <p><a href="{{route('login')}}" class="text-white py-3 px-4 btn btn-primary btn-sm">Login Now</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="site-blocks-cover overlay" style="background-image: url(page/images/hero_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-7 text-center" data-aos="fade">
                        <span class="caption mb-3">Deliverance Church International</span>
                        <h1 class="mb-4">Church of choice transforming Nations for Christ</h1>
                        <p><a href="{{route('login')}}" class="text-white py-3 px-4 btn btn-primary btn-sm">SignIn</a></p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="py-5 upcoming-events" style="background-color: #1594A8">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <span class="caption mb-3 " style="background-color: orangered">Our Mission:</span>
                    <h6 class="text-white">Life celebration Center exist to reconcile people to God,Equip the for ministry to God
                        and the people with transformative agender</h6>
                </div>
                <div class="col-md-4">
                    <span class="caption mb-3  px-4" style="background-color: orangered">Our Vission:</span>
                    <h6 class="text-white">"Church of Choice,Transforming Nation <br/>for Christ"</h6>
                </div>
                <div class="col-md-4">
                    <span class="caption mb-3 px-4" style="background-color: orangered">Our Core Values:</span>
                    <h6 class="text-white">
                        <div class="row">
                            <div class="col-sm-6">
                                *Hospitality<br/>
                                *Integrity<br/>
                                *ServeantHood<br/>
                                *Fellowship
                            </div>
                            <div class="col-sm-6">
                                *Approved<br/>
                                *Commitment<br/>
                                *Excellent<br/>
                                <i>'We Care'</i>
                            </div>
                        </div>


                    </h6>
                </div>

            </div>
        </div>
    </div>


    <div class="py-5 quick-contact-info">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center">
                    <div>
                        <a href="#" data-toggle="modal" data-target="#map"><span class="icon-room text-white h2 d-block"></span></a>
                        <h2>Location</h2>
                        <p class="mb-0">Deliverance Church Nakuru Main Church <br> Along General Kariba Street</p>
                        <p class="mb-0">Nakuru - 2398 <br></p>
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <div>
                        <span class="icon-clock-o text-white h2 d-block"></span>
                        <h2>Service Times</h2>
                        <p class="mb-0">Tuesday 5:30 to 6:30 Bible Study/Home Church <br>
                            Wednesday: 4:00pm to 6.00pm  <br>
                            Thursday: 5:30 to 6:30 JC momentum Prayer/Choir Practise<br>
                            Friday: 3:00pm to 6:30 Prayers for ALL. SUNDAY: 9am to 12pm</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="site-footer" style="background-image: url('page/images/hero_1.jpg');">
        <div class="container">
            <div class="row pt-5 mt-5 text-center">
                <div class="col-md-12">
                    <p>

                        Copyright &copy; <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">

                        </script><script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">

                        </script><script type="8c1a3e3b0166727b4bda798a-text/javascript">document.write(new Date().getFullYear());</script> All Rights Reserved | Life celebration church</i></a>

                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="page/js/jquery-3.3.1.min.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/jquery-migrate-3.0.1.min.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/jquery-ui.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/popper.min.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/bootstrap.min.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/owl.carousel.min.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/jquery.stellar.min.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/jquery.countdown.min.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/jquery.magnific-popup.min.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/bootstrap-datepicker.min.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/aos.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/mediaelement-and-player.min.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script src="page/js/main.js" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script type="8c1a3e3b0166727b4bda798a-text/javascript">
      document.addEventListener('DOMContentLoaded', function() {
                var mediaElements = document.querySelectorAll('video, audio'), total = mediaElements.length;

                for (var i = 0; i < total; i++) {
                    new MediaElementPlayer(mediaElements[i], {
                        pluginPath: 'https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/',
                        shimScriptAccess: 'always',
                        success: function () {
                            var target = document.body.querySelectorAll('.player'), targetTotal = target.length;
                            for (var j = 0; j < targetTotal; j++) {
                                target[j].style.visibility = 'visible';
                            }
                  }
                });
                }
            });
    </script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="8c1a3e3b0166727b4bda798a-text/javascript"></script>
<script type="8c1a3e3b0166727b4bda798a-text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js" data-cf-settings="8c1a3e3b0166727b4bda798a-|49" defer=""></script></body>
</html>
