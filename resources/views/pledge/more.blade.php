@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile" id="printElement">
                <div>
                    <div class="row justify-content-center">
                        <img src="{{asset('images/logo.jpeg')}}" class="center" style=" border-radius: 50%;">

                    </div>
                    <p class="text-info text-center">Transforming Nations for Christ</p><hr>
                    <p>
                        <strong> Member Name:   </strong> {{$pledge->member->fname}}   {{$pledge->member->lname}}<br/>
                        <strong>Phone Number:   </strong>{{$pledge->member->phone}}<br/>
                        <strong>Member Number:   </strong>{{$pledge->member->regno}}
                    </p>
                    <p class="text-center"><b>Statement for Partnership for Kingdom Harvest Account</b></p><hr>
                </div>
                <div class="tile-body">
                    @include('includes.message')
                    <table class="table table-hover table-bordered table-striped" id="table1" >
                        <thead>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Amount Pledged</th>
                        <th>Payments</th>
                        <th>Balance</th>
                        </thead>
                        <tbody>
                       <tr class="text-primary">
                           <td>{{date('d-m-Y', strtotime($pledge->date))}}</td>
                           <td>Pledge</td>
                           <td>{{number_format($pledge->amount,2)}}</td>
                           <td>{{number_format($pledgedata->sum('amount'),2)}}</td>
                           <td>{{number_format($pledge->amount-$pledgedata->sum('amount'),2)}}</td>
                       </tr>
                        @foreach($pledgedata as $data)
                            <tr>
                                <td>{{date('d-m-Y', strtotime($data->date))}}</td>
                                <td>{{$data->type}}</td>
                                <td></td>
                                <td>{{number_format($data->amount,2)}}</td>
                                <td>{{number_format($pledge->amount-($pledgedata->where('date','<=',$data->date)->sum('amount')),2)}}</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>
                <div class="tile-footer">
                    <p class="text-center">Cheques Payable to <strong>Deliverance Ministry Team.</strong> Mpesa Paybill No. <strong>882784</strong></p>
                    <p class="text-center">Thank you for Your Continuous Support .</p>
<hr>
                    <div class="bs-component">
                        <div class="progress mb-2">
                            @if(number_format(($pledgedata->sum('amount')/$pledge->amount)*100,2)>100)
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: {{($pledgedata->sum('amount')/$pledge->amount)*100}}%">Over payment</div>
                                @else
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: {{($pledgedata->sum('amount')/$pledge->amount)*100}}%">{{number_format(($pledgedata->sum('amount')/$pledge->amount)*100,2)}}%</div>
                       @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
     <a type="button"  href="{{route('pledges')}}" class="btn btn-info fa fa-backward float-left">Pledges</a>&nbsp;&nbsp;&nbsp;
                <button   class="btn btn-secondary fa fa-print float-right" id="printButton">PDF</button>

                <button  class="btn btn-primary fa fa-file-excel-o" onclick="exportTableToExcel('tblData','{{$pledge->member->fname}} {{$pledge->member->lname}}')">Excel</button>
            </div>
        </div>
    </div>
@endsection
