@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <div>
                    <p id="dataname">All church Pledges</p>
                </div>
                <div class="tile-body">
                    @include('includes.message')
                    <table class="table table-hover table-bordered table-striped" id="table" >
                        <thead>
                        <th>#</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Pledge Type</th>
                        <th>Total Amount</th>
                        <th>Amount Paid</th>
                        <th>Balance</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach($datas as  $key=>$data)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$data->member->fname}} &nbsp;&nbsp;&nbsp; {{$data->member->lname}}</td>
                                <td>{{$data->member->phone}}</td>
                                <td>{{$data->type}}</td>
                                <td>{{number_format($data->amount,2)}}</td>
                                <td>{{number_format($data->pledge_data->sum('amount'),2)}}</td>
                                @if($data->amount-$data->pledge_data->sum('amount')>0)

                                <td class="text-primary">{{number_format($data->amount-$data->pledge_data->sum('amount'),2)}}</td>
                                    @elseif($data->amount-$data->pledge_data->sum('amount')<0)
                                    <td class="text-danger">{{number_format($data->amount-$data->pledge_data->sum('amount'),2)}}</td>
                                @else
                                    <td>{{number_format($data->amount-$data->pledge_data->sum('amount'),2)}}</td>
                                    @endif
                                <td>{{date('d-m-Y', strtotime($data->date))}}</td>
                                @if($data->amount>$data->pledge_data->sum('amount'))
                                <td style="color: red;">Unpaid</td>
                                    @else
                                    <td style="color: grey;">Paid</td>
                                    @endif
                                <td><a class="btn btn-info btn-sm fa fa-money" href="#" data-toggle="modal" data-target="#{{$data->id}}">Pay</a> </td>
                                <td><a class="btn btn-primary btn-sm fa fa-eye" href="{{route('more',$data->id)}}">View</a> </td>
                            </tr>
@include('modals.pay')
                            @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
