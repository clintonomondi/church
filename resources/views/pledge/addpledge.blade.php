@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="tile">
                <h6 class="tile-title">Record Pledges</h6>
                <div class="tile-body">
                @include('includes.message')
                <!-- Nav tabs -->
                    <form method="post" action="{{route('pledges')}}">
                        @csrf
                        <div class="form-group">
                            <label for="sel1">Pledger:</label>
                            <select class="form-control" id="selUser" name="member_id" required>
                                <option value='0'>-- Select user --</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Pledge Type:</label>
                            <select class="form-control" id="sel1" name="type">
                                <option disabled="disabled" selected="selected">Pledge type</option>
                                <option>PKH Pledge</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Date</label>
                            <input class="form-control"  type="date" placeholder="Select Date" name="date">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Amount</label>
                            <input class="form-control" name="amount" type="number" placeholder="Enter amount" required>
                        </div>
                        <div class="tile-footer">
                            <button class="btn btn-primary"  type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>&nbsp;&nbsp;
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection
