@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile" id="printElement">
                <div>
                    <div class="row justify-content-center">
                        <img src="{{asset('images/logo.jpeg')}}" class="center" style=" border-radius: 50%;">
                    </div>
                    <p class="text-center">{{$member->fname}}   {{$member->lname}}</p>
                </div>
                <div class="tile-body">
                    @include('includes.message')
                    <table class="table table-bordered table-striped display nowrap">
                        <thead>
                        <th>Name</th>
                        <th>Details</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Full Name</td>
                            <td>{{$member->fname}}   {{$member->lname}}</td>
                        </tr>
                        <tr>
                            <td>Reg Number</td>
                            <td>{{$member->regno}}</td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>{{$member->gender}}</td>
                        </tr>
                        <tr>
                            <td>Marittal status</td>
                            <td>{{$member->marital}}</td>
                        </tr>
                        <tr>
                            <td>Date of birth</td>
                            <td>{{$member->dob}}</td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>{{$member->phone}}</td>
                        </tr>
                        <tr>
                            <td>Ministry</td>
                            <td>{{$member->ministry}}</td>
                        </tr>
                        <tr>
                            <td>Born again?</td>
                            <td>{{$member->born}}</td>
                        </tr>
                        <tr>
                            <td>Residence</td>
                            <td>{{$member->residence}}</td>
                        </tr>
                        <tr>
                            <td>Church</td>
                            <td>{{$member->church}}</td>
                        </tr>
                        <tr>
                            <td>Zone</td>
                            <td>{{$member->category->name}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{$member->email}}</td>
                        </tr>
                        <tr>
                            <td>Parent</td>
                            <td>{{$member->parent}}</td>
                        </tr>
                        <tr>
                            <td>Date adimtted</td>
                            <td>{{$member->created_at}}</td>
                        </tr>

                        </tbody>
                    </table>

                </div>


            <div class="tile-footer">
                <a type="button"  href="{{route('members')}}" class="btn btn-info fa fa-backward float-left">Members</a>&nbsp;&nbsp;&nbsp;
                <button   class="btn btn-secondary fa fa-print float-right" id="printButton">Print</button>
            </div>
        </div>
        </div>

    </div>

@endsection
