@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <div>
                    <p id="dataname">New member</p>
                </div>
                <div class="tile-body">
                    @include('includes.message')

                                        <div class="row">
                                            <div class="col-md-12 mx-0">
                                                <div id="msform">
                                                    <!-- progressbar -->
                                                    <ul id="progressbar">
                                                        <li class="active" id="account"><strong>Personal</strong></li>
                                                        <li id="personal"><strong>Church</strong></li>
                                                        <li id="payment"><strong>Parent</strong></li>
                                                        <li id="confirm"><strong>Finish</strong></li>
                                                    </ul> <!-- fieldsets -->
                                                    <form method="post" action="{{route('postmember')}}">
                                                        @csrf
                                                    <fieldset>
                                                        <div class="form-card">
                                                            <h2 class="fs-title">Personal Information</h2>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">First Name</label>
                                                                        <input class="form-control" name="fname" type="text" placeholder="Enter first name" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Last Name</label>
                                                                        <input class="form-control" name="lname" type="text" placeholder="Enter last name" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Email address</label>
                                                                        <input class="form-control" name="email" type="text" placeholder="Enter email">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="sel1">Marital status:</label>
                                                                        <select class="form-control" id="sel1" name="marital">
                                                                            <option disabled="disabled" selected="selected">Select marital status</option>
                                                                            <option>Single</option>
                                                                            <option>Married</option>
                                                                            <option>Divorced</option>
                                                                            <option>Widowed</option>
                                                                            <option>Separated</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Date of birth</label>
                                                                        <input class="form-control"  type="date" placeholder="Select Date" name="dob">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Phone Number</label>
                                                                        <input class="form-control" name="phone" type="text" placeholder="Enter phone no">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <input type="button" name="next" class="next action-button" value="Next Step" />
                                                    </fieldset>
                                                    <fieldset>
                                                        <div class="form-card">
                                                            <h2 class="fs-title">Church Information</h2>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="sel1">Gender:</label>
                                                                        <select class="form-control" id="sel1" name="gender">
                                                                            <option disabled="disabled" selected="selected">Select gender..</option>
                                                                            <option>Male</option>
                                                                            <option>Female</option>
                                                                            <option>Others</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="sel1">Area of ministry:</label>
                                                                        <select class="form-control" id="sel1" name="ministry">
                                                                            <option disabled="disabled" selected="selected">Select area of ministry..</option>
                                                                            <option>Usher</option>
                                                                            <option>Security</option>
                                                                            <option>Intercessor</option>
                                                                            <option>Choir</option>
                                                                            <option>Protocol</option>
                                                                            <option>Media</option>
                                                                            <option>Zone Pastor</option>
                                                                            <option>Ass. Zone Pastor </option>
                                                                            <option>Home Churh Pastor </option>
                                                                            <option>Ass. Home Churh Pastor </option>
                                                                            <option>Evangelism </option>
                                                                            <option>Catering </option>
                                                                            <option>Pastoral </option>
                                                                            <option>Sunday Sch. Teacher </option>
                                                                            <option>None </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="sel1">Born Again?:</label>
                                                                        <select class="form-control" id="sel1" name="born">
                                                                            <option disabled="disabled" selected="selected"> Select ...</option>
                                                                            <option>Yes</option>
                                                                            <option>No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Residence</label>
                                                                        <input class="form-control" name="residence" type="text" placeholder="Enter residence">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Home Church</label>
                                                                        <input class="form-control" name="church" type="text" placeholder="Enter home church">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="sel1">Zone:</label>
                                                                        <select class="form-control" id="zone" name="category_id" required>
                                                                            <option disabled="disabled" selected="selected">Select Zone</option>
                                                                            @foreach($zones as $zone)>
                                                                            <option value="{{$zone->id}}">{{$zone->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                                                    </fieldset>
                                                    <fieldset>
                                                        <div class="form-card">
                                                            <h2 class="fs-title">Parent Information</h2>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Parent Name(For sunday School)</label>
                                                                        <input class="form-control" name="parentName" type="text" placeholder="Enter parent name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Parent phone no(For sunday school)</label>
                                                                        <input class="form-control" name="parentPhone" type="text" placeholder="Enter parent phone">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Spouse Name</label>
                                                                        <input class="form-control" name="spouse" type="text" placeholder="Enter spouse name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="sel1">Ministerial team:</label>
                                                                        <select class="form-control" id="sel1" name="ministerial">
                                                                            <option disabled="disabled" selected="selected"> Select ministerial team</option>
                                                                            <option>DAUGHTERS OF FAITH(DOF)</option>
                                                                            <option>EAGLES MINISTRY(SOF)</option>
                                                                            <option>JOSHUA CALEB MOMENTUM (JC)</option>
                                                                            <option>SUNDAY SCHOOL(SS)</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Confirm" />
                                                    </fieldset>
                                                    <fieldset>
                                                        <div class="form-card">
                                                            <h2 class="fs-title text-center">PROCEED? !</h2> <br><br>
                                                            <div class="row justify-content-center">
                                                                <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                                                            </div> <br><br>
                                                            <div class="row justify-content-center">
                                                                <div class="col-7 text-center">
                                                                    <h5>Click submit button to submit</h5>
                                                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

@endsection
