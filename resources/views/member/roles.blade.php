@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <div>
                    <p id="dataname">Members who are supposed to login to the system with roles</p>
                </div>
                <a class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#addroles">Add Users</a>
                <div class="tile-body">
                    @include('includes.message')
                    @include('modals.addRole')
                    <table class="table table-bordered table-striped display nowrap" id="table">
                        <thead>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th></th>
                        <th></th>
                        </thead>
                        <tbody>
                        @if(count($users)>0)
                            @foreach($users as $key=>$user)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->role}}</td>
                                    <td><a class="fa fa-remove btn btn-danger btn-sm" href="{{route('removeUser',$user->id)}}" ></a> </td>
                                    <td><a class="fa fa-edit btn btn-primary btn-sm" href="#"  data-toggle="modal" data-target="#{{$user->id}}"></a> </td>
                                </tr>
                                @include('modals.editRole')
                            @endforeach
                        @else
                            <p>No records </p>
                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
