@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <div>
                    <p id="dataname">All church members</p>
                </div>
                <div class="tile-body">
                    @include('includes.message')
                    <table class="table table-hover table-bordered table-striped" id="table" >
                        <thead>
                        <th>#</th>
                        <th>Name</th>
                        <th>Reg Number</th>
                        <th>Home Church</th>
                        <th>Area  ministry</th>
                        <th>Ministerial</th>
                        <th>email</th>
                        <th>Zone</th>
                        <th>Phone</th>
                        <th></th>
                        </thead>
                        <tbody>
                            @foreach($members as $key=>$member)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$member->fname}} &nbsp;&nbsp;&nbsp; {{$member->lname}}</td>
                                    <td>{{$member->regno}}</td>
                                    <td>{{$member->church}}</td>
                                    <td>{{$member->ministry}}</td>
                                    <td>{{$member->ministerial}}</td>
                                    <td>{{$member->email}}</td>
                                    <td>{{$member->category->name}}</td>
                                    <td>{{$member->phone}}</td>
                                    <td><a class="fa fa-eye btn btn-info btn-sm" href="{{route('memberview',$member->id)}}" ></a> </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
