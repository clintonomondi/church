@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        @include('includes.message')
        @include('modals.comment')
        @include('modals.editMembers')
        <div class="col-md-6">
            <div class="tile">
                <form  method="post"  action="{{route('postSearch')}}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-3">
                            <select class="form-control" id="selUser" name="regno">
                                <option value='0'>-- Select user --</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-primary float-right" type="submit"><i class="fa fa-fw fa-lg fa-search"></i>Search</button>
                        </div>
                        <div class="col-sm-3">
                        @cannot('isQuery')
                        <a class="fa fa-edit btn btn-info btn-lg" href="#" data-toggle="modal" data-target="#{{$member->id}}">Edit</a>
                       @endcannot
                        </div>
                        <div class="col-sm-3">
                            @cannot('isQuery')
                            <a class="btn btn-secondary btn-lg fa fa-user-plus"  href="{{route('addmember')}}">Add</a>
                           @endcannot
                            </div>
                    </div>
                </form>
                <hr>
                <div class="tile-body">
                    <table class="table table-bordered table-striped display nowrap" >
                        <thead>
                        <th>Name</th>
                        <th>Details</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>First Name</td>
                            <td id="fname">{{$member->fname}}</td>
                        </tr>
                        <tr>
                            <td>Last Name</td>
                            <td id="lname">{{$member->lname}}</td>
                        </tr>
                        <tr>
                            <td>Reg Number</td>
                            <td id="regno">{{$member->regno}}</td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td id="gender">{{$member->gender}}</td>
                        </tr>
                        <tr>
                            <td>Marital status</td>
                            <td id="marital">{{$member->marital}}</td>
                        </tr>
                        <tr>
                            <td>Date of birth</td>
                            <td id="dob">{{$member->dob}}</td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td id="phone">{{$member->phone}}</td>
                        </tr>
                        <tr>
                            <td>Area of Ministry</td>
                            <td id="ministry">{{$member->ministry}}</td>
                        </tr>
                        <tr>
                            <td>Born again?</td>
                            <td id="born">{{$member->born}}</td>
                        </tr>
                        <tr>
                            <td>Residence</td>
                            <td id="residence">{{$member->residence}}</td>
                        </tr>
                        <tr>
                            <td>Home Church</td>
                            <td id="church">{{$member->church}}</td>
                        </tr>
                        <tr>
                            <td>Zone</td>
                            <td id="zone">{{$member->category->name}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td id="email">{{$member->email}}</td>
                        </tr>
                        <tr>
                            <td>Parent</td>
                            <td id="parent">{{$member->parentName}}</td>
                        </tr>
                        <tr>
                            <td>Date admitted</td>
                            <td id="date">{{$member->created_at}}</td>
                        </tr>
                        <tr>
                            <td>Comments</td>
                            <td id="comment">{{$member->comment->count()}}</td>
                        </tr>
                        <tr>
                            <td >Spouse</td>
                            <td id="spouse">{{$member->spouse}}</td>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="tile">
                <div class="row">
                    <div class="col-sm-6">
                <h3 class="tile-title">Comments</h3>
                    </div>
                    <div class="col-sm-6">
                @cannot('isQuery')
                <span class="float-lg-right"><a class="fa fa-comment btn btn-info btn-sm" data-toggle="modal" data-target="#comment">Add comment</a> </span>
                    </div>
                </div>
                @endcannot
                <div class="tile-body">
                    <table class="table table-bordered table-striped display nowrap" id="table">
                        <thead>
                        <th>#</th>
                        <th>Comment</th>
                        <th>Date</th>
                        </thead>
                        <tbody>
                        @if(count($comment)>0)
                            @foreach($comment as $key=>$com)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$com->comment}}</td>
                                    <td>{{$com->created_at}}</td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
