@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        @include('includes.message')
        <div class="col-md-12">
            <div class="tile">
                <form  method="post"  action="{{route('postSearch')}}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-4">
                            <select class="form-control" id="selUser" name="regno">
                                <option value='0'>-- Select user --</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-search"></i>Search</button>
                        </div>
                        <div class="col-sm-4">
                        @cannot('isQuery')
                        <a class="btn btn-secondary btn-lg fa fa-user-plus"  href="{{route('addmember')}}">New Member</a>
                            @endcannot
                        </div>
                    </div>
                </form>
                <hr>
                <div class="tile-body">
                    <table class="table table-bordered table-striped display nowrap" >
                        <thead>
                        <th>Name</th>
                        <th>Details</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>First Name</td>
                            <td id="fname"></td>
                        </tr>
                        <tr>
                            <td>Last Name</td>
                            <td id="lname"></td>
                        </tr>
                        <tr>
                            <td>Reg Number</td>
                            <td id="regno"></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td id="gender"></td>
                        </tr>
                        <tr>
                            <td>Marital status</td>
                            <td id="marital"></td>
                        </tr>
                        <tr>
                            <td>Date of birth</td>
                            <td id="dob"></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td id="phone"></td>
                        </tr>
                        <tr>
                            <td>Ministry</td>
                            <td id="ministry"></td>
                        </tr>
                        <tr>
                            <td>Born again?</td>
                            <td id="born"></td>
                        </tr>
                        <tr>
                            <td>Residence</td>
                            <td id="residence"></td>
                        </tr>
                        <tr>
                            <td>Home Church</td>
                            <td id="church"></td>
                        </tr>
                        <tr>
                            <td>Zone</td>
                            <td id="zone"></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td id="email"></td>
                        </tr>
                        <tr>
                            <td>Parent</td>
                            <td id="parent"></td>
                        </tr>
                        <tr>
                            <td>Date admitted</td>
                            <td id="date"></td>
                        </tr>
                        <tr>
                            <td>Comments</td>
                            <td id="comment"></td>
                        </tr>
                        <tr>
                            <td >Spouse</td>
                            <td id="spouse"></td>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                                <h3 class="tile-title">Comments</h3>
                <div class="tile-body">
                    <table class="table table-bordered table-striped display nowrap" id="table">
                        <thead>
                        <th>#</th>
                        <th>Comment</th>
                        <th>Date</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection
