@extends('layouts.app')

@section('content')
    <div class="app-title">
        <div>
            <p id="dataname">All Comments</p>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                {{--                <h3 class="tile-title">Vertical Form</h3>--}}
                <div class="tile-body">
                    @include('includes.message')
                    <table class="table table-bordered table-striped display nowrap" id="table">
                        <thead>
                        <th>#</th>
                        <th>Comment</th>
                        <th>Member</th>
                        <th>Date</th>
                        </thead>
                        <tbody>
                            @foreach($comments as $key=>$comment)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$comment->comment}}</td>
                                    <td>{{$comment->member->fname}} &nbsp;&nbsp;&nbsp; {{$comment->member->lname}}</td>
                                    <td>{{$comment->created_at}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
