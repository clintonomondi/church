<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('docs/css/main.css')}}">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Login -Church</title>
</head>
<body>
<section class="material-half-bg">
    <div class="cover " style="background: #1594A8;"></div>
</section>
<section class="login-content">
    @include('includes.message')
    <div class="login-box">
        <form class="login-form" method="post" action="{{route('password.update')}}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="row justify-content-center">
                <img src="{{asset('images/logo.jpeg')}}" class="center" style=" border-radius: 50%;">
            </div><hr>
            <p class="text-center">Please enter new password</p>
            <div class="form-group">
                <label class="control-label">EMAIL</label>
                <input class="form-control" type="text" name="email" placeholder="Username" autofocus>
            </div>
            <div class="form-group">
                <label class="control-label">PASSWORD</label>
                <input class="form-control" type="password" name="password" placeholder="Password">
            </div>
            <div class="form-group">
                <label class="control-label">CONFIRM PASSWORD</label>
                <input class="form-control" type="password" name="password_confirmation" placeholder="Confirm Password">
            </div>
            <div class="form-group btn-container">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>RESET PASSWORD</button>
            </div>
            <div class="form-group mt-3">
                <p class="semibold-text mb-0"><a href="{{route('login')}}" ><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
            </div>
        </form>
    </div>
</section>
<!-- Essential javascripts for application to work-->
<script src="{{asset('docs/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('docs/js/popper.min.js')}}"></script>
<script src="{{asset('docs/js/bootstrap.min.js')}}"></script>
<script src="{{asset('docs/js/main.js')}}"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="{{asset('docs/js/plugins/pace.min.js')}}"></script>
<script type="text/javascript">
    // Login Page Flipbox control
    $('.login-content [data-toggle="flip"]').click(function() {
        $('.login-box').toggleClass('flipped');
        return false;
    });
</script>
</body>
</html>
