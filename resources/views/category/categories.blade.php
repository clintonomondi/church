@extends('layouts.app')

@section('content')
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="tile">
                    @include('includes.message')
                    @include('modals.addCategory')
                     <p id="dataname">Zones</p>
                        <div class="col-md-2 float-right">
                            @cannot('isQuery')
                            <a class="btn btn-primary fa fa-plus" href="#" data-toggle="modal" data-target="#addZone">Add Zones</a>
                       @endcannot
                        </div>
                    <div class="tile-body">
                    <table class="table table-hover table-bordered table-striped" id="table" >
                        <thead>
                        <th>#</th>
                        <th>Category Name</th>
                        <th>Description</th>
                        <th>Zonal Pastor</th>
                        <th>Members</th>
                        <th></th>
                        @cannot('isQuery')
                        <th></th>
                            @endcannot
                        </thead>
                        <tbody>
                        @if(count($cats)>0)
                            @foreach($cats as $key=>$cat)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$cat->name}}</td>
                                    <td>{{$cat->description}}</td>
                                    <td>{{$cat->paster}}</td>
                                    <td>{{$cat->member->count()}}</td>
                                    <td>&nbsp;<a class="fa fa-eye btn btn-success btn-sm" href="{{route('getMembers',$cat->id)}}" ></a></td>
                                   @cannot('isQuery')
                                    <td><a class="fa fa-edit btn btn-primary btn-sm" href="#" data-toggle="modal" data-target="#{{$cat->id}}"></a> </td>
                               @endcannot
                                </tr>
                                @include('modals.editCategory')
                            @endforeach
                        @else
                            <p>No records </p>
                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        </div>
@endsection
