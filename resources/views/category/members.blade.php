@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <div>
                    <p id="dataname">{{$datas->name}}   Zone Members</p>
                </div>
                <div class="tile-body">
                    @include('includes.message')
                    <table class="table table-bordered table-striped display nowrap" id="table">
                        <thead>
                        <th>#</th>
                        <th>Name</th>
                        <th>Reg Number</th>
                        <th>Home Church</th>
                        <th>Area  ministry</th>
                        <th>email</th>
                        <th>Phone no</th>
                        <th>Zone</th>
                        </thead>
                        <tbody>
                            @foreach($datas->member as $key=>$member)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$member->fname}} &nbsp;&nbsp;&nbsp; {{$member->lname}}</td>
                                    <td>{{$member->regno}}</td>
                                    <td>{{$member->church}}</td>
                                    <td>{{$member->ministry}}</td>
                                    <td>{{$member->email}}</td>
                                    <td>{{$member->phone}}</td>
                                    <td>{{$member->category->name}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
