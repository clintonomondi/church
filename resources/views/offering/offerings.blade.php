@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="tile">
                <div>
                    @if(empty($to))
                        <p id="dataname">Report of All Offerings</p>
                    @else
                        <p id="dataname">Report of All Offerings from {{$from}} to {{$to}} </p>
                    @endif
                </div>
                <div class="tile-body">
                    <form method="post" action="{{route('searchofferings')}}">
                        <div class="row justify-content-center">
                            @csrf
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Period From</label>
                                    <input class="form-control" id="demoDate" type="date" placeholder="Select Date from" name="from">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Period To</label>
                                    <input class="form-control"  type="date" placeholder="Select Date to" name="to">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-search"></i>Search</button>&nbsp;
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    @include('includes.message')
                    <table class="table table-bordered table-striped display nowrap" id="table">
                        <thead>
                        <th>#</th>
                        <th>Amount(Ksh.)</th>
                        <th>Offering Type</th>
                        <th>Service</th>
                        <th>Giver</th>
                        <th>Reg no</th>
                        <th>Phone no</th>
                        <th>Date</th>
                        @cannot('isQuery')
                        <th></th>
                            @endcannot
                        </thead>
                        <tbody>
                        @if(!empty($offers))
                            @foreach($offers as $key=>$offer)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{number_format($offer->item,2)}}</td>
                                    <td>{{$offer->offeringtype}}</td>
                                    <td>{{$offer->service}}</td>
                                    @if(empty($offer->member->fname))
                                    <td>NA</td>
                                    <td>NA</td>
                                    <td>NA </td>
                                    @else
                                        <td>{{$offer->member->fname}}  {{$offer->member->lname}}</td>
                                        <td>{{$offer->member->regno}} </td>
                                        <td>{{$offer->member->phone}} </td>
                                        @endif
                                    <td>{{$offer->date}}</td>
                                    @cannot('isQuery')
                                    <td><a class="fa fa-edit btn btn-primary btn-sm" href="{{route('editoffer',$offer->id)}}"></a> </td>
                               @endcannot
                                </tr>
                            @endforeach
                        @else

                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>


@endsection
