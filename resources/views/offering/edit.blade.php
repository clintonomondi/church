@extends('layouts.app')

@section('content')
    <div class="app-title">
        <div>
            <p>Edit Offer</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{route('offerings')}}">Offerings</a></li>
        </ul>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="tile">
                {{--                <h3 class="tile-title">Vertical Form</h3>--}}
                <div class="tile-body">
                @include('includes.message')
                            <form method="post" action="{{route('updateoffer',$offer->id)}}">
                                @csrf
                                <div class="form-group">
                                    <label for="sel1">Giver:</label>
                                    <select class="form-control" id="sel1" name="member_id" required>
                                        <option value="{{$offer->member->id}}">{{$offer->member->fname}} {{$offer->member->lname}} {{$offer->member->regno}}</option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->fname}} {{$user->lname}} {{$user->regno}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="sel1">Offering Type:</label>
                                    <select class="form-control" id="sel1" name="offeringtype" required>
                                        <option value="{{$offer->offeringtype}}">{{$offer->offeringtype}}</option>
                                        <option>Tithes</option>
                                        <option>Thanks giving</option>
                                        <option>Seed of Faith</option>
                                        <option>Missions</option>
                                        <option>Building </option>
                                        <option>Prophets Honor </option>
                                        <option>Free Will Offering </option>
                                        <option>Offering </option>
                                        <option>Tv Support </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Amount/Item</label>
                                    <input class="form-control" name="item" type="text" value="{{$offer->item}}" placeholder="Enter full name" required>
                                    <input class="form-control" name="service"  value="Prayer Service" type="text" placeholder="Enter full name" hidden>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Service</label>
                                    <select class="form-control" id="sel1" name="service" required>
                                        <option value="{{$offer->service}}">{{$offer->service}}</option>
                                        <option>Prayer Service</option>
                                        <option>Bible Study</option>
                                        <option>Sunday Service</option>
                                    </select>
                                </div>
                                <div class="tile-footer">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;
                                </div>
                            </form>
                        </div>

            </div>
        </div>
    </div>


@endsection
