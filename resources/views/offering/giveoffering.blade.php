@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="tile">
                                <h6 class="tile-title">Collect offering</h6>
                <div class="tile-body">
                    @include('includes.message')
                    <!-- Nav tabs -->
                            <form method="post" action="{{route('postoffering')}}">
                                @csrf
                                <div class="form-group">
                                    <label for="sel1">Service:</label>
                                    <select class="form-control" id="sel1" name="service" required>
                                        <option></option>
                                        <option>Sunday Service</option>
                                        <option>Prayer Service</option>
                                        <option>Bible study</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                        <label for="sel1">Giver:</label>
                                    <select class="form-control" id="selUser" name="member">
                                        <option value='0'>-- Select user --</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="sel1">Offering Type:</label>
                                    <select class="form-control" id="sel1" name="offeringtype" required>
                                        <option></option>
                                        <option>Tithes</option>
                                        <option>Thanks giving</option>
                                        <option>Seed of Faith</option>
                                        <option>Missions</option>
                                        <option>Building </option>
                                        <option>Prophets Honor </option>
                                        <option>Free Will Offering </option>
                                        <option>Offering </option>
                                        <option>Tv Support </option>
                                    </select>
                                </div>
                                    <div class="form-group">
                                        <label class="control-label">Date</label>
                                        <input class="form-control"  type="date" placeholder="Select Date" name="date">
                                    </div>
                                <div class="form-group">
                                    <label class="control-label">Amount</label>
                                    <input class="form-control" name="item" type="number" placeholder="Enter amount" required>
                                </div>
                                <div class="tile-footer">
                                    <button class="btn btn-primary"  type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>&nbsp;&nbsp;
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                </div>


@endsection
